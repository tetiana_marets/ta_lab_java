package com.epam.lab.hw11.classinfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MyOwnClass{
    private static final Logger log = LogManager.getLogger(MyOwnClass.class);
    private Class cls;

    MyOwnClass(Object obj){
        cls = obj.getClass();
    }

    public String getClassName(){
        return cls.getSimpleName();
    }
    public void showInfoAboutClass(){
        log.info("Class name is {}", cls.getSimpleName());
        for (Field f: cls.getDeclaredFields()) {
            log.info("Class field: {} ",f.getName());
            log.info("Type of field is {}" ,f.getType().getSimpleName());
            log.info("Field modifiers is {}" ,f.getModifiers());
        }
        for (Method m: cls.getDeclaredMethods()) {
            log.info("Class method: {}",m.getName());
            log.info("Return type of field is {}",m.getReturnType());
            log.info("Default value is {}",m.getDefaultValue());
        }
    }
}