package com.epam.lab.hw11.annotation;

import com.epam.lab.hw11.annotation.model.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DemoAnnotation {
    private static final Logger log = LogManager.getLogger(DemoAnnotation.class);

    public static void main(String[] args) {
            String[] libraries = {"NY", "Kyiv", "Lviv"};
            Book book = new Book("Cracking the coding interview.", "Unknown", 345);
            book.setAvailableLibrary(libraries);
            ReflectionOperation reflctOperation = new ReflectionOperation();
            reflctOperation.printAnnotatedField(book);
            log.info("------------------------------");
            reflctOperation.invokeMethod(book);
            reflctOperation.invokeMyMethod(book);
    }
}
