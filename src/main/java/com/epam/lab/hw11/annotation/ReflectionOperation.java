package com.epam.lab.hw11.annotation;

import com.epam.lab.hw11.annotation.model.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ReflectionOperation {
    private static final Logger log = LogManager.getLogger(ReflectionOperation.class);

    void printAnnotatedField(Book book){
        Class cls = book.getClass();
        Field[] fields = cls.getDeclaredFields();
        for (Field field: fields) {
            if (field.isAnnotationPresent(CustomAnnotation.class)) {
                CustomAnnotation customAnnotation = field.getAnnotation(CustomAnnotation.class);
                log.info("Field is annotated: {}",field.getName());
                log.info("Name value in @MyCustomAnnotation = {}",customAnnotation.name());
                log.info("isPublished value in @MyCustomAnnotation = {}",customAnnotation.isPublished());
            }
            else {
                log.debug("Field is NOT annotated: {}",field.getName());
            }
        }
    }
    void invokeMethod(Book book){
        String[] methodName = {"getName", "getPageNumber", "getAvailableLibrary"};
        Class cls = book.getClass();
        Method[] methods = cls.getDeclaredMethods();
        for (Method method: methods) {
            log.debug("Return type of {} is {}",method.getName(),method.getReturnType().getSimpleName());
        }
        for (String s: methodName) {
            try {
                Method method = cls.getDeclaredMethod(s);
                method.setAccessible(true);
                method.invoke(book);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                log.info(e.getMessage());
            }
        }
    }
    void invokeMyMethod (Book book) {
        Class cls = book.getClass();
        try {
            Method methodInt = cls.getDeclaredMethod("myMethod", String.class, int[].class);
            methodInt.setAccessible(true);
            methodInt.invoke(book, "test", new int[]{1, 10, 3});

            Method methodStr = cls.getDeclaredMethod("myMethod", String[].class);
            methodStr.setAccessible(true);
            methodStr.invoke(book, (Object) new String[]{"No", "time", "for", "losers"});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
