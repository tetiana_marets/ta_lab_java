package com.epam.lab.hw11.annotation.model;

import com.epam.lab.hw11.annotation.CustomAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Book {
    private static final Logger log = LogManager.getLogger(Book.class);

    @CustomAnnotation(name = "Little women") private String name;
    private String author;
    @CustomAnnotation(isPublished = false) private int pageNumber;
    @CustomAnnotation private String[] availableLibrary;

    public Book(String name, String author, int pageNumber){
        this.name = name;
        this.author = author;
        this.pageNumber = pageNumber;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
    public void setAvailableLibrary(String[] availableLibrary){
        this.availableLibrary = availableLibrary;
    }
    public void myMethod(String a, int...args){
        log.info("Method with String and int varargs");
        for (int i: args) {
            log.info("i = {} + {}", i, a);
        }
    }
    void myMethod(String...args){
        log.info("Method with String varargs");
        for (String s: args) {
            log.info(s);
        }
    }
    public String getName() {
        log.info("Name of the book is = {} ",name);
        return name;
    }
    public String[] getAvailableLibrary() {
        log.info("Book is available in the following libraries = {} " , Arrays.toString(availableLibrary));
        return availableLibrary;
    }
}
