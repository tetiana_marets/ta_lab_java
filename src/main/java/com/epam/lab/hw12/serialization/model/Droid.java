package com.epam.lab.hw12.serialization.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    DroidType droidType;

    public Droid(String name, DroidType droidType) {
        this.name = name;
        this.droidType = droidType;
    }

    @Override
    public String toString() {
        return "Name { " + name + " }"
                + " Type {" + droidType.toString() + " }";
    }
}