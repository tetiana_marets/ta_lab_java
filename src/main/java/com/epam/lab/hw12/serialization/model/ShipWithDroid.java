package com.epam.lab.hw12.serialization.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShipWithDroid implements Serializable {
    private List<Droid> droidList;
    private String shipName;
    private transient int numberOfDroids ;

    public ShipWithDroid(String shipName){
        this.shipName = shipName;
        droidList = new ArrayList<>();
    }
    public void addDroid(Droid droid){
        droidList.add(droid);
        numberOfDroids = droidList.size();
    }

    @Override
    public String toString() {
        return "The ship { " + shipName + " } have droids { "
                + droidList.toString() + " } "
                + " The numberOfDroids(transient) = " + numberOfDroids;
    }
}

