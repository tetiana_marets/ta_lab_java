package com.epam.lab.hw12.serialization;

import com.epam.lab.hw12.serialization.model.ShipWithDroid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Deserialization {
    private static final Logger log = LogManager.getLogger(Deserialization.class);
    private static final String PATH = "./src/main/resources/droidShip.ser";

    static void demo() {
        ShipWithDroid shipWithDroid = null;
        try(
                FileInputStream fileInput = new FileInputStream(PATH);
                ObjectInputStream objInput = new ObjectInputStream(fileInput);
                ) {
            shipWithDroid = (ShipWithDroid) objInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        log.info("Deserialize Ship with Droid:");
        log.info(shipWithDroid.toString());
    }
}
