package com.epam.lab.hw12.serialization;

import com.epam.lab.hw12.serialization.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Serialization {
    private static final Logger log = LogManager.getLogger(Serialization.class);
    private static final String PATH = "./src/main/resources/droidShip.ser";
    static void demo() {
        ShipWithDroid shipWithDroid = new ShipWithDroid("Serializable");
        shipWithDroid.addDroid(new Droid("Droid1", DroidType.BATTLE));
        shipWithDroid.addDroid(new Droid("Droid2", DroidType.PILOT));
        try (
                FileOutputStream fileOut = new FileOutputStream(PATH,false);
                ObjectOutputStream out =  new ObjectOutputStream(fileOut)) {
            out.writeObject(shipWithDroid);
            log.info("Serialized data is saved into /src/main/resources");
            log.info(shipWithDroid.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
