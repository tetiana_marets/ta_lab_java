package com.epam.lab.hw12.directorycontent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

class FilesWalk {
    private static final Logger log = LogManager.getLogger(FilesWalk.class);

    static void filesWalkExt(Path path) throws IOException {
        FileVisitor<Path> myFileVisitor = new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs){
                log.debug(dir);
                return FileVisitResult.CONTINUE;
            }
            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs){
                File f = new File(path.toString());
                log.info("  {}",path);
                log.debug("  attributes: length = {}. isHidden = {}. R/W  = {} / {}"
                        ,f.length() ,f.isHidden(), f.canRead(),f.canWrite());

                return FileVisitResult.CONTINUE;
            }
        };
        Files.walkFileTree(path, myFileVisitor);
    }
}
