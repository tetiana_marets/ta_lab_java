package com.epam.lab.hw12.directorycontent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class CommandLines {
    private static final Logger log = LogManager.getLogger(CommandLines.class);

    public static void Demo () {
        PathFinder pathFinder = new PathFinder();
        Path path = Paths.get(pathFinder.getRoot());
        log.info("You root directory is {}", pathFinder.getRoot());
        String command;
        Scanner sc = new Scanner(System.in);
        do {
            log.info("Please enter your command (cd/dir/exit):");
            command = sc.nextLine().trim();
            if (command.startsWith("dir")){
                try{
                    FilesWalk.filesWalkExt(path);
                } catch (IOException e) {
                    log.info(e.getMessage());
                }
            }
            if (command.startsWith("cd")){
                String filePath = command.replace("cd","").trim();
                if (Files.exists(Paths.get(filePath))){
                    path = Paths.get(filePath);
                    log.info("You are in the {}",path.toString());
                } else {
                    log.info("This directory does not exist");
                }

            }
        } while (!command.equals("exit"));
    }
}