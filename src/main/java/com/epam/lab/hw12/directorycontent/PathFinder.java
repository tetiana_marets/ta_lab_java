package com.epam.lab.hw12.directorycontent;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

class PathFinder {
    private static final String PATH_TO_PROPERTIES_FILE = "./src/main/resources/path.properties";
    private Properties properties;

    PathFinder()  {
        properties = new Properties();
    }

    private Properties getProperties(){
        try {
            properties.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    String getRoot(){
        return getProperties().getProperty("root");
    }

}
