package com.epam.lab.hw12.removecomment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ReadFromFile {
    final static String PATH_TO_PROPERTIES_FILE = "./src/main/resources/path.properties";

    static String getPathToFile(){
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  properties.getProperty("removeComments");
    }
    static List<String> readFile(String path) {
        List<String> listOfLines = new ArrayList<>();
        try (Scanner sc = new Scanner(new File(path))) {
            while (sc.hasNext()) {
                listOfLines.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return listOfLines;
    }
}
