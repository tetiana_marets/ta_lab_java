package com.epam.lab.hw12.removecomment;

import java.util.ArrayList;
import java.util.List;

class RetrieveComment {
    private List<String> sourceList;
    private List<String> listOfComments;
    private int newIndex;

    RetrieveComment (List<String> sourceList) {
        this.sourceList = sourceList;
        listOfComments = new ArrayList<>();
    }
    List<String> comments() {
        for (int i = 0; i < sourceList.size()-1; i++){
            singleLineComment(i);
            multiLineComment(i);
                if ( i != newIndex) { i = newIndex; }
            }
        return listOfComments;
    }
    private void singleLineComment (int indexOfLine) {
        String line = sourceList.get(indexOfLine).trim();
        if (line.startsWith("//")){
            listOfComments.add(line);
        } else if (line.contains("//")){
            String subString = line.substring(line.indexOf("//"));
            if (!(subString.contains("*/") || subString.contains(Character.toString((char)34)))){
                listOfComments.add(subString);
            }
        }
    }
    private void multiLineComment (int indexOfLine) {
        String line = sourceList.get(indexOfLine).trim();
        newIndex = indexOfLine;
            if (line.startsWith("/**") ||
                    line.startsWith("/*")) {
                do {
                   listOfComments.add(sourceList.get(newIndex).trim());
                    newIndex++;
                }
                while (!(sourceList.get(newIndex).trim().endsWith("*/")));
                listOfComments.add(sourceList.get(newIndex));
            }
    }
}