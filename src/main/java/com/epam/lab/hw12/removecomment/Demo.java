package com.epam.lab.hw12.removecomment;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Demo {
    private static final Logger log = LogManager.getLogger(Demo.class);

    public static void main(String[] args) {
        List<String> sourceLisOfLines = ReadFromFile.readFile(ReadFromFile.getPathToFile());
        RetrieveComment retrieveComment = new RetrieveComment(sourceLisOfLines);
        for (String s: retrieveComment.comments()) {
            log.info(s);
        }
    }
}
