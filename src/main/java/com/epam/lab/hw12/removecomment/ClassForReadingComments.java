package com.epam.lab.hw12.removecomment;

/** javadoc
 javadoc comment
 for testing
 */

//One more single line comment
public class ClassForReadingComments {
    //  It's a single line comment
    String name;
    int count;
    ClassForReadingComments(){
        /*multiline
        comment
         */
        name = "Class//Name";
    }
    /** javadoc
     javadoc comment
     for testing
     */
    public void setCount(int count) {
        this.count = count;//it's a comment after ;
        System.out.println("Testing // comment");
    }
}
