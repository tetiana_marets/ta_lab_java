package com.epam.lab.hw12.hugefile;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.concurrent.TimeUnit;

class FileOperations {
    private static final Logger log = LogManager.getLogger(FileOperations.class);
    private static final String PATH = "./src/main/resources/200mb.txt";
    private File hugeFile;
    File usualWriter() {
        log.debug("Writing with usual Writer");
        hugeFile = new File(PATH);
        StopWatch stopWatch = StopWatch.createStarted();
        try(FileOutputStream fileOut = new FileOutputStream(hugeFile,true)){
            writeToFile(fileOut);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        stopWatch.stop();
        log.info("Time = " + stopWatch.getTime(TimeUnit.MILLISECONDS));
        return hugeFile;
    }
    File bufferedWriter(){
        log.debug("Writing with buffered Writer");
        hugeFile = new File(PATH);
        StopWatch stopWatch = StopWatch.createStarted();
        try(
                FileOutputStream fileOut = new FileOutputStream(hugeFile,true);
                BufferedOutputStream buffOut = new BufferedOutputStream(fileOut)
        ){
            writeToFile(buffOut);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        stopWatch.stop();
        log.info("Time = " + stopWatch.getTime(TimeUnit.MILLISECONDS));
        return hugeFile;
    }
    private void writeToFile(OutputStream outputStream) throws IOException {
        Faker faker = new Faker();
        while (hugeFile.length() < 20000000){
            byte[] information = faker.address().fullAddress().getBytes();
            outputStream.write(information);
        }
    }
    void deleteFile(File file) {
        if (file.delete()){
            log.info("File is deleted successfully.");
        } else {
            log.info("File isn't deleted.");
        }
    }
    void usualReader(File file) {
        log.debug("Test with usual reader.");
        StopWatch stopWatch = StopWatch.createStarted();
        try (InputStream inStream = new FileInputStream(file)){
            readData(inStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stopWatch.stop();
        log.info("Time = " + stopWatch.getTime(TimeUnit.MILLISECONDS));
    }
    void bufferedReader (File file) {
        log.debug("Test with buffered reader");
        StopWatch stopWatch = StopWatch.createStarted();
        try (
                FileInputStream inStream = new FileInputStream(file);
                BufferedInputStream buffInStream = new BufferedInputStream(inStream);
        ){
            readData(buffInStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stopWatch.stop();
        log.info("Time = " + stopWatch.getTime(TimeUnit.MILLISECONDS));
    }
    private void readData(InputStream inputStream) throws IOException {
        int data = inputStream.read();
        while (data != -1){
            data = inputStream.read();
        }
    }
}
