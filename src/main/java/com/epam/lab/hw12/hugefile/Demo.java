package com.epam.lab.hw12.hugefile;

import java.io.File;

public class Demo {
    public static void main(String[] args) {
        File file;
        FileOperations fileOperations = new FileOperations();
        file = fileOperations.usualWriter();
        fileOperations.deleteFile(file);
        file = fileOperations.bufferedWriter();
        fileOperations.usualReader(file);
        fileOperations.bufferedReader(file);
        fileOperations.deleteFile(file);
    }
}
