package com.epam.lab.hw10.texttask.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sentence implements Comparable<Sentence>{
    private static final String SPLIT_SENTENCE = "(\\s)|((?<=[,.!?:;])|(?=[,.!?:;]))";
    private static final String VOWELS = "(?i)\\b[aeiou]\\S*";
    private static final  String DELIMITER = " ";

    private List<SentenceElement> sentenceElementList;
    private int wordCount;

    Sentence(String sentence){
        sentenceElementList = new ArrayList<>();
        splitSentenceOnElements(sentence);
    }

    private void splitSentenceOnElements(String sentence){
        String[] splitSentence = sentence.split(SPLIT_SENTENCE);
        for (String s: splitSentence){
            SentenceElement element = new SentenceElement(s);
            if (element.isWord()){
                wordCount++;
            }
            sentenceElementList.add(element);
        }
    }

    public List<String> getAllWords(){
        List<String> listWords = new ArrayList<>();
        for (SentenceElement s: sentenceElementList){
            if (s.isWord()){
                listWords.add(s.element);
            }
        }
        return listWords;
    }

    private String getLongestWord(){
        return getAllWords().stream().max(Comparator.comparing(String::length)).get();
    }

    public String replaceWordStartFromVowelToLongest(){
        return toString().replaceFirst(VOWELS,getLongestWord());
    }

    public long numberOfSameWords(){
        int sameWordCount = 0;
        List<String> listOfSameWords = new ArrayList<>();
        for (String s: getAllWords()){
            if (!listOfSameWords.contains(s.toLowerCase())){
                listOfSameWords.add(s.toLowerCase());
            }
            else{
                sameWordCount++;
            }
        }
        return sameWordCount;
    }

    @Override
    public String toString() {
        String output = sentenceElementList.stream().map(s->s.toString()).collect(Collectors.joining(DELIMITER));
        return Stream.of(output).map(s -> s.replace(" .", "."))
                .map(s -> s.replace(" !", "!"))
                .map(s -> s.replace(" ?", "?"))
                .collect(Collectors.joining());
    }

    @Override
    public int compareTo(Sentence o) {
        return  this.wordCount - o.wordCount;
    }
}