package com.epam.lab.hw10.texttask.models;

import java.util.ArrayList;
import java.util.List;

public class Text {
    private static final String SPLIT_TEXT = "(?<=[.!?])\\s";
    List<Sentence> sentenceList;

    public Text(String sourceText){
        sentenceList = new ArrayList<>();
        splitText(sourceText);
    }

    void splitText(String sourceText){
        String[] splitText = sourceText.split(SPLIT_TEXT);
        for (String s: splitText){
            Sentence sentence = new Sentence(s);
            sentenceList.add(sentence);
        }
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public int getNumberOfSentences(){
        return sentenceList.size();
    }

    @Override
    public String toString() {
        return sentenceList.toString();
    }
}
