package com.epam.lab.hw10.texttask.models;

import java.util.regex.Pattern;

public class SentenceElement {
    private static final String REGEX_WORD = "\\w+";
    private static final String REGEX_PUNCTUATION = "[,;:?!.]";

    String element;

    SentenceElement(String element){
        this.element = element;
    }

    boolean isWord(){
        return Pattern.compile(REGEX_WORD).matcher(element).matches();
    }

    boolean isPunctuation(){
        return Pattern.compile(REGEX_PUNCTUATION).matcher(element).matches();
    }

    @Override
    public String toString() {
        return element;
    }
}
