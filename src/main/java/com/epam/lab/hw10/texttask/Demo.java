package com.epam.lab.hw10.texttask;

import com.epam.lab.hw10.texttask.logic.TextOperations;
import com.epam.lab.hw10.texttask.models.Sentence;
import com.epam.lab.hw10.texttask.models.Text;
import com.epam.lab.hw10.texttask.reader.ReadFromFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Demo {
    private static final Logger log = LogManager.getLogger(Demo.class);

    public static void main(String[] args) {
        String sourceText = ReadFromFile.readTextFromFile();
        Text text = new Text(sourceText);

        System.out.println(text.getSentenceList().get(0).numberOfSameWords());

        log.info("Text is sorted by word's count (ASC order)");
        log.info(TextOperations.wordCountAsc(text).toString());

        log.info("Word starts from vowel replaced to the longest one");
        for (Sentence s: text.getSentenceList()) {
            System.out.println(s.replaceWordStartFromVowelToLongest());
        }

        log.info("Unique word from first sentence is");
        log.info(TextOperations.uniqueWordFromFirstSentence(text));

        log.info("All words sorted alphabetically");
        log.info(TextOperations.sortWordsAlphabetically(text).toString());

        log.info(" ------ task 4 --------");
        log.info(TextOperations.getWordOfSetLength(text).toString());
    }
}
