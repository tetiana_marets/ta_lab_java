package com.epam.lab.hw10.texttask.logic;

import com.epam.lab.hw10.texttask.models.Sentence;
import com.epam.lab.hw10.texttask.models.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class TextOperations {
    private static final Logger log = LogManager.getLogger(TextOperations.class);

    public static List<Sentence> wordCountAsc(Text text){
        List<Sentence> sortedList = text.getSentenceList();
        Collections.sort(sortedList);
        return sortedList;
    }

    public static List<String>  getWordOfSetLength(Text text){
        log.info("Please enter length of the word.");
        Scanner sc = new Scanner(System.in);
        int wordLength = 0;
        try{
            wordLength = sc.nextInt();
        } catch (IllegalArgumentException e){
            e.getMessage();
        }
        List<String> questionSentenceWord = new ArrayList<>();
        for (Sentence sentence : text.getSentenceList()) {
            if (sentence.toString().endsWith("?")){
                questionSentenceWord.addAll(sentence.getAllWords());
            }
        }
        int finalWordLength = wordLength;
        return questionSentenceWord.stream().filter(s->(s.length() == finalWordLength)).distinct().collect(Collectors.toList());
    }

    public static String uniqueWordFromFirstSentence(Text text){
        String uniqueWord = "";
        String remainingText = text.getSentenceList().subList(1,text.getNumberOfSentences()).toString().toLowerCase();
        for (String word: text.getSentenceList().get(0).getAllWords()) {
            if (!remainingText.contains(word.toLowerCase())){
                uniqueWord = word;
                break;
            }
            else {
                uniqueWord = "There is no such word.";
            }
        }
        return uniqueWord;
    }

    public static List<String> sortWordsAlphabetically(Text text){
        List<String> sortedList = new ArrayList<>();
        for (Sentence sentence: text.getSentenceList()) {
            sortedList.addAll(sentence.getAllWords());
        }
        return sortedList.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    }

}

