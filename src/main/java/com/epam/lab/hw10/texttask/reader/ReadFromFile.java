package com.epam.lab.hw10.texttask.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFromFile {
    private static final String PATH = "./src/main/resources/textBigTask.txt";

    public static String readTextFromFile(){
        StringBuilder dataSource = new StringBuilder();
        try (Scanner sc = new Scanner(new File(PATH))){
            while (sc.hasNextLine()){
                dataSource.append(sc.nextLine());
            }
        }
        catch (FileNotFoundException e){
            e.getMessage();
        }
        return dataSource.toString().trim();
    }
}
