package com.epam.lab.hw10.splitstring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class SplitString {
    private static final String SPLIT = "(?<=(the)|(you))";
    private static final String ALL_VOWELS = "[aouei]";
    private static final String FIRST_UPPER_CASE_END_FULL_STOP = "[A-Z][^\\.]*[\\.$]";

    private static final Logger log = LogManager.getLogger(SplitString.class);
    private static String str = "The code for strings is held in a file aptly named string. " +
            "As we learned with the “Hello World” program, you can use the C++ Standard Library.";

    public static void main(String[] args) {
        splitString();
        replaceAllVowelsWithUnderscore();
    }
    private static void splitString(){
        String[] stringArr = str.split(SPLIT);
        for (String s: stringArr) {
            log.info(s);
        }
    }
    private static void replaceAllVowelsWithUnderscore(){
        String replacedStr = str.replaceAll(ALL_VOWELS,"_");
        log.info(replacedStr);
    }
}
