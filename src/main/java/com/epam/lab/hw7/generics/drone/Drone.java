package com.epam.lab.hw7.generics.drone;

class Drone {
    private String name;
    Drone(){

    }
    Drone(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return  "Drone{ " +
                "name = " + this.getName();
    }
}
