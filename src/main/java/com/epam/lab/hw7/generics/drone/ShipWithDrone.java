package com.epam.lab.hw7.generics.drone;

import java.util.List;

class ShipWithDrone < T extends PrimitiveDrone>{
    private T drone;

    public void setDrone(T drone) {
        this.drone = drone;
    }

     <E extends PrimitiveDrone> void addDroneToList (List<? super PrimitiveDrone> listOfDrone, E drone){
         listOfDrone.add(drone);
    }

    @Override
    public String toString() {
        return "Drone{ "
                 + "name = " + drone.getName()
                 + " shipping weight = " + drone.shippingWeight
                 + "}";
    }
}