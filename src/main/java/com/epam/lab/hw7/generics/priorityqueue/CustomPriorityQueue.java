package com.epam.lab.hw7.generics.priorityqueue;

import com.epam.lab.hw7.generics.drone.PrimitiveDrone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

class CustomPriorityQueue<T extends PrimitiveDrone & Comparable<PrimitiveDrone> >{
   private static final Logger log = LogManager.getLogger(CustomPriorityQueue.class);
   private Object[] queue;
   private int size;

    public CustomPriorityQueue(){
        queue = new Object[0];
        size = 0;
    }

    public void add(T e){
        if (e == null){
            throw new NullPointerException();
        }
        Object[] extendedQueue = Arrays.copyOf(queue,size+1);
        extendedQueue[size] = e;
        queue = extendedQueue;
        Arrays.sort(queue);
        size++;
    }

     void remove(T e){
        int index = Arrays.binarySearch(queue,e);
        removeByIndex(index);
    }

    private void removeByIndex(int index){
        if (index < 0 || index > size){
            log.info("Element is not present in the queue ");
        }
        else
            {
            Object[] newQueue = new Object[size-1];
            int j = 0;
            for (int i =0; i < size; i++){
                if (index == i) {
                    continue;
                }
                newQueue[j] = queue[i];
                j++;
            }
            queue = newQueue;
            size--;
        }
    }

    public int size() {
        return size;
    }

    public T poll() {
        T fElement = (T) queue[0];
        try{
            peek();
            return fElement;
        }
        catch (Exception e){
            return null;
        }
    }

    private T peek() {
        try{
            return (T)queue[0];
        }
        catch (Exception e){
            return null;
        }
    }

    Iterator<T> iterator() {
        return new CustomPriorityQueueIterator<>();
    }

    @SuppressWarnings("TypeParameterHidesVisibleType")
    private class CustomPriorityQueueIterator<T> implements Iterator<T>{
        int currentPosition = 0;
        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public T next() {
            if (!hasNext()){
                throw new NoSuchElementException();
            }
            else{
                T value = (T) queue[currentPosition];
                currentPosition++;
                return value;
            }
        }
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomPriorityQueue<?> that = (CustomPriorityQueue<?>) o;
        return size == that.size &&
                Arrays.equals(queue, that.queue);
    }

    public int hashCode() {
        int result = Objects.hash(size);
        result = 31 * result + Arrays.hashCode(queue);
        return result;
    }

    public String toString() {
        return  "Priority Queue { " + Arrays.toString(queue) + " }";
    }

}
