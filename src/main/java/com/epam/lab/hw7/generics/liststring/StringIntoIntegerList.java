package com.epam.lab.hw7.generics.liststring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

class StringIntoIntegerList {
    private static final Logger log = LogManager.getLogger(StringIntoIntegerList.class);

    private static void addToList(List list, String s){
        list.add(s);
    }

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        addToList(integerList,"test");
        integerList.add(25);
        log.info(integerList.get(0));
        log.info(integerList.toString());
    }
}
