package com.epam.lab.hw7.generics.priorityqueue;

import com.epam.lab.hw7.generics.drone.PrimitiveDrone;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

class MainPriorityQueue {
    private static final Logger log = LogManager.getLogger(MainPriorityQueue.class);

    public static void main(String[] args) {
        CustomPriorityQueue<PrimitiveDrone> customQueue = new CustomPriorityQueue<>();
        Iterator<PrimitiveDrone> it = customQueue.iterator();

        PrimitiveDrone drone1 = new PrimitiveDrone("drone1", 2800);
        PrimitiveDrone drone2 = new PrimitiveDrone("drone2", 1100);
        PrimitiveDrone drone3 = new PrimitiveDrone("drone3", 50);
        PrimitiveDrone drone4 = new PrimitiveDrone("drone4", 3500);
        customQueue.add(drone1);
        customQueue.add(drone2);
        customQueue.add(drone3);
        customQueue.add(drone4);
        log.info("Initial queue");
        log.info(customQueue.toString() + "\n");

        log.info("Queue after deleting Drone2. Iterating by using Iterator.");
        customQueue.remove(drone2);
         while (it.hasNext()){
            log.info(it.next());
        }

    }
}
