package com.epam.lab.hw7.generics.drone;

public class CarrierDrone extends PrimitiveDrone {
    private final Type type = Type.MULTI_ROTOR;

    public CarrierDrone(String name, double shippingWeight){
        super(name,shippingWeight);
    }

    @Override
    public String toString() {
        return "Drone{ " +
                 "name = " + this.getName()
                + ", type = " + this.type
                  + ", max shipping weight = " +  this.shippingWeight + "}";
    }
}
