package com.epam.lab.hw7.generics.drone;

import java.util.Objects;

public class PrimitiveDrone extends Drone implements Comparable<PrimitiveDrone>{
    private static final Type type = Type.SINGLE_ROTOR;
    private String name;
    double shippingWeight;

    public PrimitiveDrone(String droidName, double shippingWeight){
        super(droidName);
        this.shippingWeight = shippingWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimitiveDrone that = (PrimitiveDrone) o;
        return Double.compare(that.shippingWeight, shippingWeight) == 0 &&
                Objects.equals(name, that.name);
    }

    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(PrimitiveDrone e) {
        return Double.compare(this.shippingWeight, e.shippingWeight);
    }

    @Override
    public String toString() {
        return "Drone{ " +
                "name = " + this.getName()
                + ", type = " + type
                + ", max shipping weight = " +  this.shippingWeight + "}";
    }

}
