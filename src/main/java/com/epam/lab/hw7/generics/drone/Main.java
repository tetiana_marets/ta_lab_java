package com.epam.lab.hw7.generics.drone;

import java.util.ArrayList;
import java.util.List;

class Main {
    public static void main(String[] args) {
    Quadcopters quadcopters = new Quadcopters("Test1Quad");
    CarrierDrone carrierDrone1 = new CarrierDrone("TestCarrDrone1", 3400);
    CarrierDrone carrierDrone2 = new CarrierDrone("TestCarrDrone2",1800);
    CarrierDrone carrierDrone3 = new CarrierDrone("TestCarrDrone3",2000);

    ShipWithDrone<PrimitiveDrone> shipWithDrone = new ShipWithDrone<>();
    shipWithDrone.setDrone(carrierDrone1);
    shipWithDrone.setDrone(carrierDrone2);

    List<PrimitiveDrone> primitiveDroneList = new ArrayList<>();
    shipWithDrone.addDroneToList(primitiveDroneList,new PrimitiveDrone("PrimDrone1", 1000));
    shipWithDrone.addDroneToList(primitiveDroneList, carrierDrone1);

    System.out.println(primitiveDroneList);

    }
}
