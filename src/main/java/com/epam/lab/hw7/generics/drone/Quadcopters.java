package com.epam.lab.hw7.generics.drone;

public class Quadcopters extends Drone{
    private static final Type type = Type.MULTI_ROTOR;

    Quadcopters(String name){
        super(name);
    }

    @Override
    public String toString() {
        return "Drone{ " +
                "name = " + this.getName()
                + ", type = " + type;
    }
}
