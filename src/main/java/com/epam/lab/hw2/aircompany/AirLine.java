package com.epam.lab.hw2.aircompany;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AirLine {
    private String name;

    List<PassengerAirCraft> listOfPassengerAirCrafts;
    List<CargoAirCraft> listOfCargoAirCrafts;
    List<AirCraft> listOfAllAirCrafts;

    public AirLine(String name){
        this.name = name;
        this.listOfPassengerAirCrafts = new ArrayList<>();
        this.listOfCargoAirCrafts = new ArrayList<>();
        this.listOfAllAirCrafts = new ArrayList<>();
    }
    public void changeName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void addPassengerAirCraft(PassengerAirCraft airCraft){
        listOfPassengerAirCrafts.add(airCraft);
        listOfAllAirCrafts.add(airCraft);
    }
    public void removePassengerAirCraft(PassengerAirCraft airCraft){
        listOfPassengerAirCrafts.remove(airCraft);
        listOfAllAirCrafts.remove(airCraft);
    }
    public void addCargoAirCrafts(CargoAirCraft airCraft){
        listOfCargoAirCrafts.add(airCraft);
        listOfAllAirCrafts.add(airCraft);
    }
    public void removeCargoAirCrafts(CargoAirCraft airCraft){
        listOfCargoAirCrafts.remove(airCraft);
        listOfAllAirCrafts.remove(airCraft);
    }
    private int totalNumberOfPassengers() {
        int totalNumberOfPassengers = 0;
        for (PassengerAirCraft listOfPassengerAirCraft : listOfPassengerAirCrafts) {
            totalNumberOfPassengers += listOfPassengerAirCraft.getNumberOfPassenger();
        }
        return totalNumberOfPassengers;
    }
    private double totalCarryingCapacity(){
        double totalCarryingCapacity = 0;
        for (CargoAirCraft listOfCargoAirCraft : listOfCargoAirCrafts) {
            totalCarryingCapacity += listOfCargoAirCraft.getCarryingCapacity();
        }
        return totalCarryingCapacity;
    }
    @Override
    public String toString() {
        return "Airline name is " + name + '\n'
                + "Total number of passengers is: " + totalNumberOfPassengers() + '\n'
                + "Total number of carrying capacity is: " + totalCarryingCapacity() + '\n';

    }

    public void sortByDistance() {
         Collections.sort(listOfAllAirCrafts);
    }
}
