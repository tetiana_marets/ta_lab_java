package com.epam.lab.hw2.aircompany;

public abstract class AirCraft implements Comparable<AirCraft>{
    Manufacture manufacture;
    private double fuelConsumption;
    private double maxFuelCapacity;
    public int flightDistance;

    public Manufacture getManufacture() {
        return manufacture;
    }

    public void setFuelParameters(double fuelConsumption, double maxFuelCapacity){
        this.fuelConsumption = fuelConsumption;
        this.maxFuelCapacity = maxFuelCapacity;
        this.flightDistance = (int) (this.maxFuelCapacity / this.fuelConsumption);
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public double getMaxFuelCapacity() {
        return maxFuelCapacity;
    }

    public double getFlightDistance() {
        return flightDistance;
    }

    @Override
    public int compareTo(AirCraft airCraft){
        return (this.flightDistance - airCraft.flightDistance);
    }

    public enum Manufacture{
        Boeing, Airbus, Bombardier, Antonov,
    }
}
