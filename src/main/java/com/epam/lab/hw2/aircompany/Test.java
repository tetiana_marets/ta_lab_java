package com.epam.lab.hw2.aircompany;

import java.util.Scanner;

class Test {
    private static AirLine airLine;

    public static void main(String[] args) {
        airLine = new AirLine("LOT");

        PassengerAirCraft p1 = new PassengerAirCraft(AirCraft.Manufacture.Boeing);
        airLine.addPassengerAirCraft(p1);
        p1.setFuelParameters(5.94,25816);
        PassengerAirCraft p2 = new PassengerAirCraft(AirCraft.Manufacture.Bombardier);
        airLine.addPassengerAirCraft(p2);
        p2.setFuelParameters(2.21, 8000);
        PassengerAirCraft p3 = new PassengerAirCraft(AirCraft.Manufacture.Airbus);
        airLine.addPassengerAirCraft(p3);
        p3.setFuelParameters(3.44,18592);

        CargoAirCraft c1 = new CargoAirCraft(AirCraft.Manufacture.Boeing);
        airLine.addCargoAirCrafts(c1);
        c1.setFuelParameters(6.3, 25816);
        CargoAirCraft c2 = new CargoAirCraft(AirCraft.Manufacture.Antonov);
        airLine.addCargoAirCrafts(c2);
        c2.setFuelParameters(9.8, 30000);
        CargoAirCraft c3 = new CargoAirCraft(AirCraft.Manufacture.Airbus);
        airLine.addCargoAirCrafts(c3);
        c3.setFuelParameters(2.95,18592);

        System.out.println(airLine);

        System.out.println("Please enter number for fuel consumption:");
        Scanner sc = new Scanner(System.in);
        double FuelConsumption = sc.nextDouble();
        sc.nextLine();
        fuelConsumption(FuelConsumption);
        airLine.sortByDistance();
        for (AirCraft airCraft: airLine.listOfAllAirCrafts){
            System.out.println("Flight distance of the " + airCraft.manufacture + " is "
                    + airCraft.flightDistance + " km.");
        }
    }

    private static void fuelConsumption(double FuelConsumption){
        boolean isFound = false;
        for (int i = 0; i < airLine.listOfPassengerAirCrafts.size(); i++){
            if (airLine.listOfPassengerAirCrafts.get(i).getFuelConsumption() == FuelConsumption){
                System.out.println(airLine.listOfCargoAirCrafts.get(i).getManufacture()
                            + " has expected fuel consumption. \n");
                isFound = true;
            }
        }
        if (isFound == false){
            System.out.println("Unfortunately, there is no aircraft in airline with expected fuel consumption.\n" );
        }

    }

}
