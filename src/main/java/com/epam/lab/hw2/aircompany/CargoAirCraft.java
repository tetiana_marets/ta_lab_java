package com.epam.lab.hw2.aircompany;

class CargoAirCraft extends AirCraft {
    private double carryingCapacity;

    public CargoAirCraft(Manufacture manufacture) {
        this.manufacture = manufacture;
        switch (manufacture) {
            case Boeing:
                this.carryingCapacity = 134.200;
                break;
            case Airbus:
                this.carryingCapacity = 70.000;
                break;
            case Antonov:
                this.carryingCapacity = 250.000;
                break;
            case Bombardier:
                this.carryingCapacity = 0;
                break;
        }
    }

    public double getCarryingCapacity(){
        return carryingCapacity;
    }
}