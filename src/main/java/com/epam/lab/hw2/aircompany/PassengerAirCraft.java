package com.epam.lab.hw2.aircompany;

class PassengerAirCraft extends AirCraft{
    private int numberOfPassengers;

   public PassengerAirCraft(Manufacture manufacture){
        this.manufacture = manufacture;
        switch (manufacture){
            case Boeing:
                this.numberOfPassengers = 451;
                break;
            case Airbus:
                this.numberOfPassengers = 525;
                break;
            case Bombardier:
                this.numberOfPassengers = 50;
                break;
            case Antonov:
                this.numberOfPassengers = 0;
                break;
        }
    }

    public int getNumberOfPassenger() {
       return numberOfPassengers;
    }
}