package com.epam.lab.hw14.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileOperation {
    private static final String PATH_TO_PROPERTIES_FILE = "./src/main/resources/path.properties";

    public String getPathToJsonData(){
        Properties property = new Properties();
        try {
            property.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        }catch (IOException e) {
            e.printStackTrace();
        }
        return property.getProperty("json_file");
    }

    public String getPathToJsonSchema() {
        Properties property = new Properties();
        try {
            property.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return property.getProperty("json_schema");
    }
    public File getJsonDataFile(){
        return new File(getPathToJsonData());

    }
    public File getJsonSchemaFile(){
        return new File(getPathToJsonSchema());
    }
}
