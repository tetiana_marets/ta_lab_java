package com.epam.lab.hw14.parser;

import com.epam.lab.hw14.model.Bank;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JsonOperation {
    private static final Logger log = LogManager.getLogger(JsonOperation.class);
    private ObjectMapper objMapper;

    public JsonOperation(){
        objMapper = new ObjectMapper();
    }

    public List<Bank> getListOfBanks(File jsonFile){
        Bank[] banks = new Bank[0];
        try{
            banks = objMapper.readValue(jsonFile,Bank[].class);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return Arrays.asList(banks);
    }

    public String writeToFile(List<Bank> banks,File jsonFile){
        try{
            objMapper.writeValue(jsonFile,banks);
            return "File " + jsonFile.getName() + " was created";
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    public boolean isJsonValid(File jsonFile, File jsonSchema){
        ProcessingReport report = null;
        boolean result = false;
        try {
            log.info("Applying schema {} to data from {}", jsonSchema.getName(),jsonFile.getName());
            JsonNode schemaNode = JsonLoader.fromFile(jsonSchema);
            JsonNode jsonData = JsonLoader.fromFile(jsonFile);
            JsonSchemaFactory jsonFactory = JsonSchemaFactory.byDefault();
            JsonSchema schema = jsonFactory.getJsonSchema(schemaNode);
            report = schema.validate(jsonData);
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        return report.isSuccess();
    }
}
