package com.epam.lab.hw14;

import com.epam.lab.hw14.model.Bank;
import com.epam.lab.hw14.model.BankComparator;
import com.epam.lab.hw14.parser.FileOperation;
import com.epam.lab.hw14.parser.JsonOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class DemoJson {
    private static final Logger log = LogManager.getLogger(DemoJson.class);
    public static void main(String[] args) {
        FileOperation fileOperation = new FileOperation();
        JsonOperation jsonOperation = new JsonOperation();
        List<Bank> banks = jsonOperation.getListOfBanks(fileOperation.getJsonDataFile());
        Collections.sort(banks,new BankComparator());
        for (Bank bank:banks){
            log.info(bank.toString());
        }
        log.info(jsonOperation.isJsonValid(fileOperation.getJsonDataFile(),fileOperation.getJsonSchemaFile()));
    }
}
