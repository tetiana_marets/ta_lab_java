package com.epam.lab.hw14.model;

public enum DepositType {
    SAVINGS,
    FIXED,
    RECURRING
}
