package com.epam.lab.hw14.model;

import java.util.List;

public class Bank {
    private String country = "CANADA";
    private String name;
    private List<Deposit> deposits;
    double totalAmount;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
        totalAmount = getTotalAmount(this.deposits);
    }

    public double getTotalAmount(List<Deposit> deposits){
        for (Deposit deposit: deposits){
            totalAmount += deposit.amount;
        }
        return totalAmount;
    }

    @Override
    public String toString() {
        return "Bank name { " + name + " }\n"
                + "Country { " + country + " }\n"
                + "List of deposits { " + deposits.toString() + " }" + "\n";
    }
}
