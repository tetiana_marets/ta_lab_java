package com.epam.lab.hw14.model;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {
    @Override
    public int compare(Bank o1, Bank o2) {
        int totalAmountCompare = (int) (o2.totalAmount - o1.totalAmount);
        int numberOfDepositCompare = o1.getDeposits().size() - o2.getDeposits().size();
        if (totalAmountCompare == 0){
            return numberOfDepositCompare;
        }
        else {
            return totalAmountCompare;
        }
    }
}