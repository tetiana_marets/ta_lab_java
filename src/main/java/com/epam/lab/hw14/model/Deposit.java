package com.epam.lab.hw14.model;

public class Deposit {
    DepositType depositType;
    String depositor;
    String accountID;
    Double amount;
    Double profitability;
    Integer timeConstraints;

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    public Integer getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(Integer timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public String toString() {
        return "Deposit {" +
                "depositType = " + depositType +
                ", depositor = '" + depositor + '\'' +
                ", accountID = '" + accountID + '\'' +
                ", amount = " + amount +
                ", profitability = " + profitability +
                ", timeConstraints = " + timeConstraints +
                " }\n";
    }
}
