package com.epam.lab.hw4.exceptions.plants;

import java.util.ArrayList;
import java.util.List;

class TestPlantExceptions {
    private static Plants addPlant(int size, Color color, Type type) throws ColorException, TypeException{
        Plants plant = new Plants(size, color, type);
        return plant;
    }

    public static void main(String[] args) throws ColorException, TypeException {
        List<Plants> plants = new ArrayList<>();
        // Correct plants
        plants.add(addPlant(10, Color.WHITE,Type.HERBS));
        plants.add(addPlant(24, Color.BLUE, Type.FLOWER));
        plants.add(addPlant(1,  Color.GREEN,Type.HOUSEPLANT));
        plants.add(addPlant(100, Color.ORANGE, Type.GRASSES));
        for (Plants p: plants) {
            System.out.println(p);
        }
    }
}
