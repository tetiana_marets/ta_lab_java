package com.epam.lab.hw4.exceptions.plants;

public enum Type {
    TREES,
    HERBS,
    GRASSES,
    HOUSEPLANT,
    FLOWER
}
