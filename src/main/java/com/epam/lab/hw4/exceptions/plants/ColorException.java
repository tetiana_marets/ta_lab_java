package com.epam.lab.hw4.exceptions.plants;

class ColorException extends Exception {
    public ColorException (String message){
        super(message);
    }
}
