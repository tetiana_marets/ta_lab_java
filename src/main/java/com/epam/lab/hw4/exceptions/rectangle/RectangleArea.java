package com.epam.lab.hw4.exceptions.rectangle;

import java.util.InputMismatchException;
import java.util.Scanner;

class RectangleArea{
    public static void main(String[] args) {
        int a;
        int b;
        int area;
        try {
            System.out.println("Please enter length and width of rectangle:");
            Scanner sc = new Scanner(System.in);
            System.out.println("Length is: ");
            a = sc.nextInt();
            sc.nextLine();
            System.out.println("Width is: ");
            b = sc.nextInt();
            sc.nextLine();
            System.out.println("Area of rectangle is: " +  new RectangleArea().squareRectangle(a,b));

        }
        catch (InputMismatchException e){
            System.out.println("Please only positive integer numbers.");
        }
    }
    private int squareRectangle(int a, int b){
         try {
             if ((a < 0) || (b<0))
                 throw new Exception ("Please only positive integer numbers.");
         }
         catch (Exception e){
             System.out.println(e.getMessage());
             return 0;
         }
         return a * b;
    }
}
