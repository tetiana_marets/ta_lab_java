package com.epam.lab.hw4.exceptions.plants;

class Plants {
    private int size;
    private Color color;
    private Type type;

    public Plants(int size, Color color, Type type) throws ColorException, TypeException {
        this.size = size;
        try{
            this.color = color;
        }
        catch (IllegalArgumentException e){
            throw new ColorException("Chosen color is not in the list.");
        }
        try {
            this.type = type;
        }
        catch (IllegalArgumentException e){
            throw new TypeException("Chosen type is not in the list.");
        }
    }
    @Override
    public String toString(){
        return "Plant type is: " + type + "\n"
                + "Plant size is: " + size + " meter\n"
                + "Plant color is: " + color + "\n";
    }
}
