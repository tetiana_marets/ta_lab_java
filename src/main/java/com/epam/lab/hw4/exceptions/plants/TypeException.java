package com.epam.lab.hw4.exceptions.plants;

class TypeException extends Exception {
    public TypeException(String message){
        super(message);
    }
}
