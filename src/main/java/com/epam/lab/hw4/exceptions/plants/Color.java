package com.epam.lab.hw4.exceptions.plants;

public enum Color {
    RED,
    BLUE,
    ORANGE,
    GREEN,
    WHITE
}
