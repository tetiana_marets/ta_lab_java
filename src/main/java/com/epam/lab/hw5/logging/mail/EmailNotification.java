package com.epam.lab.hw5.logging.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class EmailNotification {
    private static final Logger log = LogManager.getLogger(EmailNotification.class);

    public static void main(String[] args) {
        try {
            throw new Exception("It's generated exception for testing mail appender.");
        }
        catch (Exception exObj){
            log.trace("This is trace message for testing mail appender.");
            log.info("This is info message for testing mail appender.");
            log.error(exObj.getMessage());
            log.fatal(exObj.getMessage());
        }
    }
}