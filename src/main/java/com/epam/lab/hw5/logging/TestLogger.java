package com.epam.lab.hw5.logging;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

class TestLogger
{
    private static final Logger log = LogManager.getLogger(TestLogger.class);

    public static void main(String[] args) {

         log.trace("Entering application");
         log.debug("This is debug message");
         log.info("This is info message.");
         log.warn("Warning message");
         log.error("Error message");
         log.fatal("Fatal message");

    }
}
