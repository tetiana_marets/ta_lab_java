package com.epam.lab.hw5.logging.sms;

import java.io.Serializable;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name ="SMS", category = "Core", elementType = "appender", printObject = true)
public final class SmsAppender extends AbstractAppender {
    private SmsAppender(String name, Filter filter,
                        Layout<? extends Serializable> layout, final boolean ignoreExceptions){
        super(name,filter,layout,ignoreExceptions);
    }

    @Override
    public void append(LogEvent event){
        try{
            SmsSender.send(new String(getLayout().toByteArray(event)));
        }
        catch (Exception exObj) {

        }
    }
     @PluginFactory
     public static SmsAppender createAppender(
             @PluginAttribute("name") String name,
             @PluginElement("Layout") Layout<? extends Serializable> layout,
             @PluginElement("Filter") final Filter filter){
        if (name == null) {
            LOGGER.error("No name provided for my Custom SMS Appender");
            return null;
        }
        if (layout == null){
            layout = PatternLayout.createDefaultLayout();
        }
        return new SmsAppender (name, filter, layout, true);
     }
}


