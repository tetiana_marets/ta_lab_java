package com.epam.lab.hw5.logging.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

class SmsSender {
    private static final String ACCOUNT_SID = "AC834abbe9b8bc10fb1e4496af651a327d";
    private static final String ACCOUNT_TOKEN = "c62230698be589bcaf41ab6e157e7072";

    public static void send (String str){
        Twilio.init(ACCOUNT_SID, ACCOUNT_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380632644823"),new PhoneNumber("+14434264107"), str).create();
    }
}
