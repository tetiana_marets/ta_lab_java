package com.epam.lab.hw5.logging.rollingfile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Log4jRollingExample {
    private static final Logger log = LogManager.getLogger(Log4jRollingExample.class);

    public static void main(String[] args) throws InterruptedException{
        for (int i = 0; i < 200; i++){
            log.info("This is the " + i + " time I logged a message.");
            Thread.sleep(100);
        }
    }
}
