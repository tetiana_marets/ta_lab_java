package com.epam.lab.hw5.logging.sms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Test {
    private static final Logger log = LogManager.getLogger(Test.class);

    public static void main(String[] args) {
        try{
            throw new Exception("This is generated exception for testing purpose.");
        }
        catch (Exception exObj){
            log.info(exObj.getMessage());
            log.error(exObj.getMessage());
            log.fatal(exObj.getMessage());
        }
    }
}