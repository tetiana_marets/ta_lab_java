package com.epam.lab.hw3.TTT;

class Game {
    final Board board;
    private CellState currentPlayer;

    public Game(){
        board = new Board();
        currentPlayer = CellState.X;
    }

    public boolean hasWon(){
        return (checkRowsForWins(board) || checkColumnsForWins(board) || CheckDiagonalsForWins(board));
    }

    private boolean checkRowsForWins(Board board){
        for (int i = 0; i < Board.ROWS; i++){
            if (checkRow(board.cells[i][0], board.cells[i][1], board.cells[i][2]) == true){
                return true;
            }
        }
        return false;
    }

    private boolean checkColumnsForWins(Board board){
        for (int j = 0; j < Board.COLUMNS; j++){
            if (checkRow(board.cells[0][j], board.cells[1][j], board.cells[2][j]) == true){
                return true;
            }
        }
        return false;
    }

    private boolean CheckDiagonalsForWins(Board board){
        if (checkRow(board.cells[0][0], board.cells[1][1], board.cells[2][2]) ||
                checkRow(board.cells[0][2], board.cells[1][1], board.cells[2][0])){
            return true;
        }
        return false;
    }

    private boolean checkRow(Cell c1, Cell c2, Cell c3){
        return ( (c1.content != CellState.EMPTY) && (c1.content == c2.content) && (c2.content == c3.content));
    }

    public void changePlayer(){
        if (currentPlayer == CellState.X){
            currentPlayer = CellState.O;
        }
        else {
            currentPlayer = CellState.X;
        }
    }

    public CellState getCurrentPlayer() {
        return currentPlayer;
    }
}
