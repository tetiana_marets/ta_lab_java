package com.epam.lab.hw3.TTT;

class Cell {
    CellState content;

    public Cell() {
        clear();
    }

    private void clear(){
        content = CellState.EMPTY;
    }

    public void paint(){
        switch (content){
            case X: {
                System.out.print(" X ");
                break;
            }
            case O:{
                System.out.print(" O ");
                break;
            }
            case EMPTY:{
                System.out.print("   ");
                break;
            }
        }

    }
}
