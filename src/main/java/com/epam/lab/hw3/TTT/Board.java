package com.epam.lab.hw3.TTT;

class Board {
    static final int ROWS = 3;
    static final int COLUMNS = 3;

    Cell[][] cells;
    public Board(){
        cells = new Cell[3][3];
        for (int i = 0; i < ROWS; i++){
            for (int j = 0; j < COLUMNS; j++){
                cells[i][j] = new Cell();
                cells[i][j].content = CellState.EMPTY;
            }
        }
    }

    public boolean isFull(){
        for (int i = 0; i < ROWS; i++){
            for (int j = 0; j < COLUMNS; j++){
                if (cells[i][j].content == CellState.EMPTY){
                    return false;
                }
            }
        }
        return true;
    }

    public void paint(){
        for (int i = 0; i < ROWS; i++){
            for (int j = 0; j < COLUMNS; j++){
                cells[i][j].paint();
                if (j < COLUMNS -1){
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i < ROWS -1 ) {
                System.out.println("-----------");
            }
        }
    }

    public boolean placeMark(int row, int col, CellState currentPlayer){
        if ((row >= 0) && (row < 3)) {
            if ((col >= 0) && (col < 3)) {
                if (cells[row][col].content == CellState.EMPTY) {
                    cells[row][col].content = currentPlayer;
                    return true;
                }
            }
        }
        return false;
    }
}
