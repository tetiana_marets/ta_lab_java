package com.epam.lab.hw3.TTT;

import java.util.Scanner;

class TicTacToe {
    private Game game;
    private CellState currentPlayer;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Game game = new Game();
        System.out.println("New Tic-Tac-Toe game!");
        do{
            System.out.println("Current board layout:");
            game.board.paint();
            System.out.println("X starts first.");
            int row;
            int column;
            do{
                System.out.println("Player " + game.getCurrentPlayer() + " enter your move (row[1-3] column[1-3]): ");
                row = sc.nextInt() - 1;
                column = sc.nextInt() - 1;
            }
            while (!game.board.placeMark(row,column,game.getCurrentPlayer()));
                game.changePlayer();
        }
        while (!game.hasWon() && !game.board.isFull());

        if (game.board.isFull() && !game.hasWon()){
            System.out.println("The game is tie.");
        }
        else {
            System.out.println("Current board layout is:");
            game.board.paint();
            game.changePlayer();
            System.out.println("CONGRATULATIONS! " + game.getCurrentPlayer() + " WINS.");
        }
    }
}
