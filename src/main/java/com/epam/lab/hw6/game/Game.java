package com.epam.lab.hw6.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

class Game {
    private static final int DOORS_COUNT = 10;
    private static final Logger log = LogManager.getLogger(Game.class);
    Door[] doors;
    private Hero hero;
    private int deadCount;

    public Game(){
        deadCount = 0;
        hero = new Hero();
        doors= new Door[DOORS_COUNT];
        doors = Door.initDoors(DOORS_COUNT);
    }

    public void start(){
        for (int i = 0; i < doors.length; i++) {
            Set<Integer> usedDoors = new HashSet<>();
            usedDoors.add(i);
            int currentLive = doors[ i ].getValue() + hero.getPower();
            boolean liveConfirmed = travelDoors(doors, usedDoors, currentLive);
            if (!liveConfirmed) {
                deadCount++;
            }
        }
    }

    private boolean travelDoors (Door[] doors, Set<Integer> usedDoors, int currentLive){
        if (currentLive < 0){
            return false;
        }
        if (usedDoors.size()  == doors.length){
            return true;
        }
        for (int i = 0; i < doors.length; i++){
            if (usedDoors.contains(i)){
                continue;
            }
            currentLive += doors[i].getValue();
            usedDoors.add(i);
            boolean liveConfirmed = travelDoors(doors,usedDoors,currentLive);
            usedDoors.remove(i);
            currentLive -= doors[i].getValue();
            if (liveConfirmed){
                return true;
            }
        }
        return false;
    }

    public void numberOfWinDoors(){
        log.info("Number of win doors is: " + (DOORS_COUNT - deadCount));
    }

    public void winPath(Set<Integer> usedDoors){
        log.info("Path to win (number of doors)" + usedDoors.toString());

    }
}
