package com.epam.lab.hw6.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class MainClass {
    private static final Logger log = LogManager.getLogger(MainClass.class);

    public static void main(String[] args) {
           Game game = new Game();
          for (Door e: game.doors){
              log.info(e.toString());
          }
          log.info("\n");
          game.start();
          game.numberOfWinDoors();
        }
}

