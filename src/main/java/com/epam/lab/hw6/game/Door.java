package com.epam.lab.hw6.game;

import java.util.Random;

public class Door {
    private static final int MIN_ARTIFACT = 10;
    private static final int MAX_ARTIFACT = 80;
    private static final int MIN_MONSTER = 5;
    private static final int MAX_MONSTER = 100;

    private final int value;
    private final int index;

    private Door(int index, int value){
        this.index = index;
        this.value = value;
    }

    public static Door[] initDoors(int doorsCount){
        Door[] doors = new Door[doorsCount];
        for (int i = 0; i < doorsCount; i++){
            doors[i] = new Door(i,random());
        }
        return doors;
    }

    private static int random(){
        Random r = new Random();
        boolean isArtifact = r.nextBoolean();
        if (isArtifact){
            return r.nextInt((MAX_ARTIFACT - MIN_ARTIFACT) + 1) + MIN_ARTIFACT;
        }
        else {
            return -1 * r.nextInt((MAX_MONSTER - MIN_MONSTER) + 1) + MIN_MONSTER;
        }
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Door { " +
                "index = " + index +
                ", value = " + value +
                " }";
    }

}
