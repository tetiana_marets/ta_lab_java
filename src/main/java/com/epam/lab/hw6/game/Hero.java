package com.epam.lab.hw6.game;

class Hero {
     private static final int POWER = 25;

     private int power;

     public Hero(){
        this.power = POWER;
     }

     public int getPower() {
          return power;
     }
}
