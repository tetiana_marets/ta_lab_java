package com.epam.lab.hw6.owndeque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

class MainOwnDeque {
    private static final Logger log = LogManager.getLogger(MainOwnDeque.class);

    public static void main(String[] args) {
        OwnDeque<Integer> ownDeque= new OwnDeque<>();
        ownDeque.addFirst(1);
        ownDeque.addFirst(2);
        ownDeque.addLast(3);
        ownDeque.add(4);
        ownDeque.add(5);
        log.info("Initial queue:");
        log.info(ownDeque.toString());
        log.info("Deque after removing first and last elements.");
        ownDeque.removeFirst();
        ownDeque.removeLast();
        log.info(ownDeque.toString());
        ownDeque.peek();

        log.info("Iterating by using iterator.");
        Iterator<Integer> iterator= ownDeque.iterator();
        while (iterator.hasNext()){
            log.info(iterator.next());
        }
    }
}
