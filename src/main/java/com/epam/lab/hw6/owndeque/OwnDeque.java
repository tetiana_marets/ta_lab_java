package com.epam.lab.hw6.owndeque;

import java.util.*;

class OwnDeque<T> extends AbstractQueue<T> {
    private Object[] elements;
    private int size;
    private Object head;

    public OwnDeque() {
        elements = new Object[0];
        size = 0;
    }

    public void addFirst(T e){
            if (e == null) {
                throw new NullPointerException();
            }
            Object [] extendedElements = Arrays.copyOf(elements,size+1);
            extendedElements[0] = e;
            for (int i = 1; i <= size; i++){
                extendedElements[i] = elements[i-1];
            }
            elements = extendedElements;
            head = elements[0];
            size++;
        }

    public void addLast(T e){
        if (e == null) {
            throw new NullPointerException();
        }
        Object [] extendedElements = Arrays.copyOf(elements,size+1);
        extendedElements[size] = e;
        elements = extendedElements;
        size++;
    }

     public void removeFirst(){
        if (elements[0] == null){
            throw new NoSuchElementException();
        }
        Object[] shortenElements = Arrays.copyOfRange(elements,1,size);
        elements = shortenElements;
        head = elements[0];
        size--;
    }

    public void removeLast(){
        if (elements[size-1] == null){
            throw new NoSuchElementException();
        }
        Object[] shortenElements = Arrays.copyOfRange(elements,0,size-1);
        elements = shortenElements;
        size--;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offer(T e) {
        addFirst(e);
        return true;
    }

    @Override
    public T poll() {
        T fElement = (T)head;
        try {
             removeFirst();
             return fElement;
        }
        catch (Exception e) {
            return null;
        }

    }

    @Override
    public T peek() {
        try {
            return (T)head;
        }
        catch (Exception e){
            return null;
        }
    }

    @Override
    public String toString() {
        return "Deque array { " + Arrays.toString(elements) +" }";
    }

    @Override
    public Iterator<T> iterator() {
        return new OwnDequeIterator();
    }

    private class OwnDequeIterator<T> implements Iterator<T>{
        int currentPosition = 0;

        @Override
        public boolean hasNext() {
            return (currentPosition < size);
        }

        @Override
        public T next() {
            if (!hasNext()){
                throw new NoSuchElementException();
            }
            T value = (T) elements[currentPosition];
            currentPosition++;
            return value;
        }
    }
}
