package com.epam.lab.hw6.stringcontainer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

class StringContainer {
    private static final Logger log = LogManager.getLogger(StringContainer.class);
    private String[] containerOfStrings;
    private int size;

    public StringContainer(){
        containerOfStrings = new String[0];
        size = 0;
    }

    boolean add (String s){
        if (containerOfStrings.length == size){
            String[] extendedArray = Arrays.copyOf(containerOfStrings,size+1);
            extendedArray[size] = s;
            containerOfStrings = extendedArray;
            size++;
        }
      return true;
    }

    String get(int index){
        try{
            if (index > containerOfStrings.length){
                throw new IndexOutOfBoundsException("Index is out of range. Please enter correct value.");
            }

        } catch (IndexOutOfBoundsException e){
            log.info(e.getMessage());
        }

        return containerOfStrings[ index-1 ];
    }

    int size(){
        return size;
    }

    @Override
    public String toString() {
        return "String container{ " + Arrays.toString(containerOfStrings)+" }";

    }
}
