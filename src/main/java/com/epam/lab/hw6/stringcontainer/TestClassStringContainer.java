package com.epam.lab.hw6.stringcontainer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

class TestClassStringContainer {
    private static final Logger log = LogManager.getLogger(TestClassStringContainer.class);
    private static final int STRING_COUNT = 1000;

    public static void main(String[] args) {
        StringContainer s = new StringContainer();
        log.info("1 element = December");
        s.add("December");
        log.info("2 element = March");
        s.add("March");
        log.info("3 element = August");
        s.add("August");
        log.info("4 element = FEBRUARY");
        s.add("FEBRUARY");
        log.info("Size of container = " + s.size());
        log.info("Container = " + s.toString());
        log.info("Second element = " + s.get(2));
        comparePerformance();
    }

    private static void comparePerformance(){
        StringContainer s = new StringContainer();
        Instant start = Instant.now();
        for (int i = 0; i<=STRING_COUNT; i++){
            s.add(Integer.toString(i));
        }
        Instant end = Instant.now();
        log.info("Time for adding 10000 strings with container = " + Duration.between(start,end).toMillis() + " milliseconds");
        List<String> arrList = new ArrayList<>();
        Instant start1 = Instant.now();
        for (int i = 0; i <= STRING_COUNT; i++){
            arrList.add(Integer.toString(i));
        }
        Instant end1 = Instant.now();
        log.info("Time for adding 10000 strings with ArrayList = " + Duration.between(start1,end1).toMillis() + " milliseconds");
    }
}
