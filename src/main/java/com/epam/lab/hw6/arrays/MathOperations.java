package com.epam.lab.hw6.arrays;

import java.util.*;

class MathOperations {
    int[] intersection(int[] arr1, int[] arr2) {
        int k = 0;
        int[] result;
        result = (arr1.length > arr2.length ? new int[arr1.length] : new int[arr2.length]);
        for (int item : arr1) {
            for (int value : arr2) {
                if (item == value) {
                    result[ k ] = item;
                    k++;
                }
            }
        }
        return Arrays.copyOf(result,k);
    }

    int[] complement(int[] arr1, int[] arr2) {
        int k = 0;
        int[] result;
        result = new int[arr1.length + arr2.length];
        for (int element : arr1) {
            boolean isPresent = false;
            for (int value : arr2) {
                if (element == value) {
                    isPresent = true;
                }
            }
            if (!isPresent) {
                result[ k ] = element;
                k++;
            }
        }
        for (int item : arr2) {
            boolean isPresent = false;
            for (int value : arr1) {
                if (item == value) {
                    isPresent = true;
                }
            }
            if (!isPresent) {
                result[ k ] = item;
                k++;
            }

        }
        return Arrays.copyOf(result,k);
    }

    int[] deleteSeries(int[] arr) {
        int i = 1;
        int j = 0;
        int [] result = new int[arr.length];
        while (i < arr.length){
            if (arr[i-1] == arr[i]){
                i++;
            }
            else {
                result[j] = arr[i-1];
                j++;
                i++;
            }
        }
        if (arr[arr.length-1] != arr[arr.length-2]){
            result[j] = arr[arr.length-1];
            j++;
        }
        return Arrays.copyOf(result,j);
    }

    int[] deleteDuplicates(int[] arr) {
        int [] result = new int[arr.length];
        Arrays.sort(arr);
        int count = 1;
        int j = 0;
        for (int i  = 0; i < arr.length-1; i++){
            if (arr[i] == arr [i+1]){
                count ++;
            }
            else if (count == 3){
                count = 1;
            }
            else {
                if (count < 3){
                    result[j] = arr[i];
                    count = 1;
                    j++;
                }
            }
        }
        if (arr[arr.length-2] != arr[arr.length-1]){
            result[j]  = arr[arr.length-1];
            j++;
        }
        return Arrays.copyOf(result,j);
    }

}

