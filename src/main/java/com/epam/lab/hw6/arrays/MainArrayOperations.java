package com.epam.lab.hw6.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

class MainArrayOperations {
    private static final Logger log = LogManager.getLogger(MainArrayOperations.class);

    public static void main(String[] args) {
        int[] array1 = {2,4,5,5,5,7,9,8,9,9};
        int[] array2 = {12,9,3,4,10,11};
        MathOperations m = new MathOperations();

        log.info("First array: " + Arrays.toString(array1));
        log.info("Second array: " + Arrays.toString(array2));
        log.info("Intersection of both arrays are: " + Arrays.toString(m.intersection(array1,array2)));
        log.info("Complement of both arrays are: " + Arrays.toString(m.complement(array1,array2)));
        log.info("1 array after deleting series of 3 elements in a raw is: " + Arrays.toString(m.deleteSeries(array1)));
        log.info("1 array after deleting duplicates elements is: " + Arrays.toString(m.deleteDuplicates(array1)));
    }
}
