package com.epam.lab.hw6.countrycapital;

import com.github.javafaker.Faker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

class CountryCapitalMainClass {
    private static final Scanner sc = new Scanner(System.in);
    private static final Logger log = LogManager.getLogger(CountryCapitalMainClass.class);
    private static final int NUMBER_OF_PAIRS = 4;

    public static void main(String[] args) {
        Faker faker = new Faker();
        CountryCapital[] arrCountryCapital = new CountryCapital[NUMBER_OF_PAIRS];
        for (int i = 0; i < arrCountryCapital.length; i++){
            arrCountryCapital[i] = new CountryCapital(faker.address().country(), faker.address().city());
        }
        log.debug("Before Sorting:");
        log.info(Arrays.toString(arrCountryCapital));

        List<CountryCapital> listCountryCapital = new ArrayList<>();

        for (CountryCapital countryCapital : arrCountryCapital) {
            listCountryCapital.add(countryCapital);
        }
        log.debug("List:");
        log.info(listCountryCapital.toString());

        log.debug(("Array after sorting by Country:"));
        Arrays.sort(arrCountryCapital);
        log.info(Arrays.toString(arrCountryCapital));

        log.debug("Array after Sorting by Capital:");
        Arrays.sort(arrCountryCapital, CountryCapital.secondStrComparator);
        log.info(Arrays.toString(arrCountryCapital));

        log.debug("List after sorting");
        Collections.sort(listCountryCapital);
        log.info(listCountryCapital.toString());

        binarySearch(listCountryCapital);
    }

    static void binarySearch(List listOfCountry){
        log.debug("Please enter Country name and Capital for binary search:");
        String countryName = sc.nextLine();
        String countryCapital = sc.nextLine();
        int index = Collections.binarySearch(listOfCountry,new CountryCapital(countryName,countryCapital),new ComparatorByCountryAndCapital());
        if (index < 0){
            log.info("Entered country is not found in the list.");
        }
        else {
            log.info("Country is in the list.");
        }
    }
}