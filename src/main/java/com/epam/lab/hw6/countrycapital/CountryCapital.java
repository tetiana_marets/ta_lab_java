package com.epam.lab.hw6.countrycapital;

import java.util.Comparator;

class CountryCapital implements Comparable<CountryCapital>{
    String country;
    String capital;

    public CountryCapital(String country, String capital){
        this.country = country;
        this.capital = capital;
    }

    @Override
    public int compareTo(CountryCapital o) {
            return this.country.compareTo(o.country);
    }

    public static Comparator<CountryCapital> secondStrComparator = new Comparator<>() {
        @Override
        public int compare(CountryCapital o1, CountryCapital o2) {
            return o1.capital.compareTo(o2.capital);
        }
    };

    @Override
    public String toString() {
        return "{ " + country
                 + " : " + capital + "}";
    }
}
