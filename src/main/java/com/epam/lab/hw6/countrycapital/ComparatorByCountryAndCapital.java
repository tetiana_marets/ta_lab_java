package com.epam.lab.hw6.countrycapital;

import java.util.Comparator;

class ComparatorByCountryAndCapital implements Comparator<CountryCapital> {
    @Override
    public int compare(CountryCapital o1, CountryCapital o2) {
        int flag  = o1.country.compareTo(o2.country);
        if (flag == 0) {
            flag = o1.capital.compareTo(o2.capital);
        }
        return flag;
    }
}
