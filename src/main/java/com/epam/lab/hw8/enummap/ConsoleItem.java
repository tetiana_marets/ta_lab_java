package com.epam.lab.hw8.enummap;

public enum ConsoleItem {
    ADD(1),
    SUBTRACT(2),
    MULTIPLY(3),
    DIVIDE(4),
    QUIT(5);

    private int value;
    ConsoleItem(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
