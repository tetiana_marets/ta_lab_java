package com.epam.lab.hw8.enummap;

import java.util.EnumMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class MyConsole {
    private static final Logger log = LogManager.getLogger(MyConsole.class);

    private final Scanner sc = new Scanner(System.in);
    private EnumMap< ConsoleItem, String> enumMap;
    private int number1;
    private int number2;

    MyConsole(){
        initMenu();
    }

    private void initMenu(){
        enumMap = new EnumMap<>(ConsoleItem.class);
        enumMap.put(ConsoleItem.ADD, "ADD (1)");
        enumMap.put(ConsoleItem.SUBTRACT, "SUBTRACT (2)");
        enumMap.put(ConsoleItem.MULTIPLY, "MULTIPLY (3)");
        enumMap.put(ConsoleItem.DIVIDE, "DIVIDE (4)");
        enumMap.put(ConsoleItem.QUIT, "QUIT (5)");
    }

    public void showMenu(){
        log.info("\nMENU");
        for (String str: enumMap.values()){
            log.info(str);
        }
    }

    public void input() {
        int choice;
        log.info("\n");
        log.info("Please enter two numbers.");
        try {
            number1 = sc.nextInt();
            number2 = sc.nextInt();
        } catch (Exception e) {
            log.info("Only integer numbers!");
        }

        try {
            do {
                log.info("Please, make your choice:");
                choice = sc.nextInt();
                if (ConsoleItem.valueOf("ADD").getValue() == choice) {
                    log.info("Result = " + (number1 + number2));
                }
                if (ConsoleItem.valueOf("SUBTRACT").getValue() == choice) {
                    log.info("Result = " + (number1 - number2));
                }
                if (ConsoleItem.valueOf("MULTIPLY").getValue() == choice) {
                    log.info("Result = " + number1 * number2);
                }
                if (ConsoleItem.valueOf("DIVIDE").getValue() == choice) {
                    if (number2 != 0) {
                        log.info("Result = " + (double) number1 / number2);
                    } else {
                        log.info("It is not allowed to divide by 0");
                    }
                }
            }
            while (choice != ConsoleItem.valueOf("QUIT").getValue());
        }
        catch (InputMismatchException e){
            log.info("Input is not correct.");
        }
    }

}
