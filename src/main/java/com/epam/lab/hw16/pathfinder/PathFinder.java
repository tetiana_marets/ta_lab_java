package com.epam.lab.hw16.pathfinder;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PathFinder {
    private static final String PATH_TO_PROPERTIES_FILE = "./src/main/resources/path.properties";
    private Properties properties;

    public PathFinder()  {
        properties = new Properties();
    }

     private Properties getProperties(){
        try {
            properties.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public String getPathToDB(){
        return getProperties().getProperty("database");
    }
    public String getUserName() {return getProperties().getProperty("db_user"); }
    public String getUserPassword() {return getProperties().getProperty("db_password");}
}
