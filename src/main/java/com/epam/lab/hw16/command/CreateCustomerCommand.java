package com.epam.lab.hw16.command;

import com.epam.lab.hw16.model.CustomerEntity;
import com.epam.lab.hw16.presenter.CustomerPresenter;
import com.epam.lab.hw16.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CreateCustomerCommand {
    private static final Logger log = LogManager.getLogger(CreateCustomerCommand.class);
    private static String EMAIL_PATTERN = "\\S+@\\S+\\.\\S+";
    private Scanner sc;
    private String customerName;
    private String customerEmail;
    private CustomerService customerService = new CustomerService();

    public void execute (){
        sc = new Scanner(System.in);
        CustomerEntity customer = new CustomerEntity();
        log.debug("Please enter unique customer name");
        customerName = sc.nextLine();
        if (customerService.ifCustomerExist(customerName)){
            int id = customerService.findCustomerByName(customerName).getId();
            log.debug("Customer with input name already exist. CustomerID = {}",id);
        }
        else {
            customer.setName(customerName);
            addEmailToCustomer(customer);
        }
    }

    private void addEmailToCustomer (CustomerEntity customer) {
        try {
            log.debug("Please enter email");
            customerEmail = sc.next(EMAIL_PATTERN);
            customer.setEmail(customerEmail);
            createCustomer(customer);
        }
        catch (InputMismatchException ex) {
            log.debug("Your input invalid email address");
        }
    }

    private void createCustomer (CustomerEntity customer){
        if (customerService.createCustomer(customer) > 0) {
            log.info("Customer has been created");
            new CustomerPresenter().showAllCustomers();
        }
    }
}
