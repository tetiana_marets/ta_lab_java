package com.epam.lab.hw16.command;

import com.epam.lab.hw16.model.FlightEntity;
import com.epam.lab.hw16.model.FlightStatus;
import com.epam.lab.hw16.presenter.AircraftPresenter;
import com.epam.lab.hw16.presenter.AirportPresenter;
import com.epam.lab.hw16.presenter.FlightPresenter;
import com.epam.lab.hw16.service.FlightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CreateFlightCommand {
    private static final Logger log = LogManager.getLogger(CreateFlightCommand.class);
    private static Scanner sc;
    private static String FLIGHT_CODE_PATTERN = "(?<![A-Z])[A-Z]{2}\\d{4}(?!\\d)";
    private FlightService flightService;
    String flightID;
    String departureAirport;
    String arrivalAirport;
    int aircraftID;

    public void execute () {
        sc = new Scanner (System.in);
        FlightEntity flight = new FlightEntity();
        try {
            log.debug("Please enter unique flight code. Code should start with two uppercase letters and 4 digits");
            flightID = sc.next(FLIGHT_CODE_PATTERN);
            flight.setId(flightID);
            addAirportsToFlight(flight);
            new FlightPresenter().showAllFlights();
        }
        catch (InputMismatchException ex) {
            log.debug("Your input does not correspond to pattern");
        }
    }

    private void addAirportsToFlight (FlightEntity flight) {
        flightService = new FlightService();
        new AirportPresenter().showAllAirports();
        log.debug("Please enter codes for departure and arrival airports accordingly");
        log.info("departure airport");
        departureAirport = sc.next().toUpperCase();
        log.info("arrival airport");
        arrivalAirport = sc.next().toUpperCase();
        if (flightService.ifAirportExist(departureAirport) && flightService.ifAirportExist(arrivalAirport)) {
            flight.setDeparture_airport_id(departureAirport);
            flight.setArrival_airport_id(arrivalAirport);
            addAircraftToFlight(flight);
        }
        else {
            log.debug("Airport with input code does not exist");
        }
    }

    private void addAircraftToFlight (FlightEntity flight) {
        new AircraftPresenter().showAllAircrafts();
        log.debug("Please enter ID of aircraft  which will serve flight");
        aircraftID = sc.nextInt();
        if (flightService.ifAircraftExist(aircraftID)) {
            flight.setAircraft_id(aircraftID);
            createFlight(flight);
        }
        else {
            log.debug("Aircraft with input ID does not exist");
        }
    }
    private void createFlight (FlightEntity flight) {
        setDefaultFlightProperties(flight);
        if (flightService.createFlight(flight) > 0 ) {
            log.debug("The flight has been created");
        }
    }
    private void setDefaultFlightProperties (FlightEntity flight){
        flight.setDeparture_time(LocalTime.parse("12:00"));
        flight.setDeparture_date(LocalDate.parse("2020-04-01"));
        flight.setStatus(FlightStatus.ON_SCHEDULE);
        flight.setGate("G0");
    }
}



