package com.epam.lab.hw16.command;

import com.epam.lab.hw16.presenter.ReservationPresenter;
import com.epam.lab.hw16.service.ReservationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class CancelReservationCommand {
    private static final Logger log = LogManager.getLogger(CancelReservationCommand.class);
    private Scanner sc;

    public void execute() {
        sc = new Scanner(System.in);
        int reservationID;
        ReservationService reservationService = new ReservationService();
        new ReservationPresenter().showAllReservations();
        log.debug("Please, enter ID of reservation which you would like to cancel");
        reservationID = sc.nextInt();
        if (reservationService.findReservationById(reservationID).getId() > 0) {
            if (reservationService.deleteReservation(reservationID) > 0) {
                log.debug("Your reservation has been deleted");
            }
        }
        else {
            log.debug("Reservation with entered ID does not exist");
        }
    }
}
