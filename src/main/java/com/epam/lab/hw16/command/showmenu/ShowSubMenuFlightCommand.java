package com.epam.lab.hw16.command.showmenu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShowSubMenuFlightCommand {
    private static final Logger log = LogManager.getLogger(ShowSubMenuFlightCommand.class);

    public void execute () {
        log.debug("FLIGHT SUB-MENU");
        log.info("[1] - Show all flights");
        log.info("[2] - Search flight by departure airport");
        log.info("[3] - Search flight by arrival airport");
        log.info("[4] - Create new flight");
        log.info("[5] - Back to main menu");
    }
}
