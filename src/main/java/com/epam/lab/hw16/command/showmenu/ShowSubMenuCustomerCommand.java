package com.epam.lab.hw16.command.showmenu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShowSubMenuCustomerCommand {
    private static final Logger log = LogManager.getLogger(ShowSubMenuCustomerCommand.class);
    public void execute () {
        log.debug("CUSTOMER SUB-MENU");
        log.info("[1] - Show all customers");
        log.info("[2] - Create new customer");
        log.info("[3] - Back to main menu");
    }
}
