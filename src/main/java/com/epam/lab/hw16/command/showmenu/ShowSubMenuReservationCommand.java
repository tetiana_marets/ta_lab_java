package com.epam.lab.hw16.command.showmenu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShowSubMenuReservationCommand {
    private static final Logger log = LogManager.getLogger(ShowSubMenuReservationCommand.class);
    public void execute () {
        log.debug("RESERVATION SUB-MENU");
        log.info("[1] - Show all reservations");
        log.info("[2] - Make a reservation");
        log.info("[3] - Cancel a reservation");
        log.info("[4] - Back to main menu");
    }
}
