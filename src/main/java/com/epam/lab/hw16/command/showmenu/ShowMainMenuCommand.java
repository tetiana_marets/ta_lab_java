package com.epam.lab.hw16.command.showmenu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShowMainMenuCommand {
    private static final Logger log = LogManager.getLogger(ShowMainMenuCommand.class);

    public void execute () {
        log.debug("MAIN MENU");
        log.info("[1] - RESERVATIONS");
        log.info("[2] - FLIGHTS");
        log.info("[3] - CUSTOMERS");
        log.info("[Q] - EXIT");
    }
}