package com.epam.lab.hw16.command;

import com.epam.lab.hw16.model.ReservationEntity;
import com.epam.lab.hw16.presenter.FlightPresenter;
import com.epam.lab.hw16.presenter.FlightSeatPresenter;
import com.epam.lab.hw16.presenter.ReservationPresenter;
import com.epam.lab.hw16.service.ReservationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MakeReservationCommand {
    private static final Logger log = LogManager.getLogger(MakeReservationCommand.class);
    private Scanner sc;
    private String flightID;
    private String customerName;
    private String seatNumber;
    private ReservationService reservationService;


    public void execute () {
        sc = new Scanner(System.in);
        reservationService = new ReservationService();
        ReservationEntity reservation = new ReservationEntity();
        new FlightPresenter().showAllFlights();
        log.info("Please enter flightID");
        flightID = sc.nextLine().toUpperCase();
        if (reservationService.checkFlightExistById(flightID)){
            flightReserve(reservation);
            new ReservationPresenter().showAllReservations();
        }
        else {
            log.info("Flight with input ID does not exist");
        }
    }
    // Aggregation: Each reservation has a flight/customer, but when reservation will be deleted - customer
    // and flight remain. 1-to-1 1 reservation may have 1 flight
    private void flightReserve(ReservationEntity reservation) {
        reservation.setFlight_id(flightID);
        log.info("Please enter customer name");
        customerName = sc.nextLine();
        if (reservationService.checkCustomerExistByName(customerName)) {
            customerReserve(reservation);
        }
        else {
            log.info("Customer with input ID does not exist");
        }
    }

    private void customerReserve(ReservationEntity reservation) {
        reservation.setCustomer_id(reservationService.getCustomerIdByName(customerName));
        new FlightSeatPresenter().showAvailableSeats(flightID);
        log.info("Please enter seat number");
        seatNumber = sc.nextLine();
        if (!reservationService.checkIfSeatReserved(seatNumber,flightID)){
            seatReserve(reservation);
        }
        else {
            log.info("Seat with input number does not exist");
        }
    }

    private void seatReserve(ReservationEntity reservation) {
        reservation.setSeat_number(seatNumber);
        reservationService.reserveSeat(seatNumber,flightID);
        if (reservationService.createReservation(reservation) > 0) {
            log.info("Your reservation is confirmed. ReservationID is {}",
                    reservationService.getReservationID(reservation));
        }
    }
}
