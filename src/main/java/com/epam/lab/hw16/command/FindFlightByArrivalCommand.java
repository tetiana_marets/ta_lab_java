package com.epam.lab.hw16.command;

import com.epam.lab.hw16.presenter.AirportPresenter;
import com.epam.lab.hw16.service.FlightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class FindFlightByArrivalCommand {
    private static final Logger log = LogManager.getLogger(FindFlightByArrivalCommand.class);

    public void execute() {
        Scanner sc = new Scanner(System.in);
        FlightService flightService = new FlightService();
        new AirportPresenter().showAllAirports();
        log.debug("Please enter code of destination airport");
        String airportCode = sc.nextLine();
        if (flightService.ifAirportExist(airportCode)) {
            if (flightService.findFlightByDestination(airportCode).isEmpty()) {
                log.debug("Unfortunately flight for input arrival airport does not exist");
            }
            else {
                log.info(flightService.findFlightByDestination(airportCode));
            }
        }
        else {
            log.debug("Airport with input code does not exist");
        }
    }
}