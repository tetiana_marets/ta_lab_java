package com.epam.lab.hw16.command;

import com.epam.lab.hw16.presenter.AirportPresenter;
import com.epam.lab.hw16.service.FlightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class FindFlightByDepartureCommand {
    private static final Logger log = LogManager.getLogger(FindFlightByDepartureCommand.class);

    public void execute () {
        Scanner sc = sc = new Scanner(System.in);
        FlightService flightService = new FlightService();
        new AirportPresenter().showAllAirports();
        log.debug("Please enter code of departure airport");
        String airportCode = sc.nextLine();
        if (flightService.ifAirportExist(airportCode)) {
            if (flightService.findFlightByDeparture(airportCode).isEmpty()) {
                log.debug("Unfortunately flight for input departure airport does not exist");
            }
            else {
                log.info(flightService.findFlightByDeparture(airportCode));
            }
        }
        else {
            log.debug("Airport with input code does not exist");
        }
    }
}
