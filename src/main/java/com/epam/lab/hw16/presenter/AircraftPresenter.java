package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.AircraftEntity;
import com.epam.lab.hw16.service.AircraftService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AircraftPresenter {
    private static final Logger log = LogManager.getLogger(AircraftPresenter.class);

    public  void showAllAircrafts() {
        AircraftService aircraftService = new AircraftService();
        if (aircraftService.getAll().isEmpty()) {
            log.debug("Aircraft list is empty");
        }
        else {
            log.debug("List of all aircrafts");
            for (AircraftEntity aircraft: aircraftService.getAll()) {
                log.info(aircraft);
            }
        }
    }
}
