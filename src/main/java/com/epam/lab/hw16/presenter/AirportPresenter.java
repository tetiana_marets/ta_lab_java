package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.AirportEntity;
import com.epam.lab.hw16.service.AirportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AirportPresenter {
    private static final Logger log = LogManager.getLogger(AirportPresenter.class);

    public  void showAllAirports () {
        AirportService airportService = new AirportService();
        if (airportService.getAll().isEmpty()) {
            log.info("Airport list is empty");
        }
        else {
            log.debug("List of all airports");
            for (AirportEntity airport: airportService.getAll()) {
                log.info(airport);
            }
        }
    }
}
