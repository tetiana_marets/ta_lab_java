package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.FlightEntity;
import com.epam.lab.hw16.service.FlightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FlightPresenter {
    private static final Logger log = LogManager.getLogger(FlightPresenter.class);

    public void showAllFlights() {
        FlightService flightService = new FlightService();
        if (flightService.findAllFlights().isEmpty()) {
            log.debug("Flight lis is empty");
        }
        else {
            log.debug("List of all flights");
            for (FlightEntity flight: flightService.findAllFlights()) {
                log.info(flight);
            }
        }
    }

}
