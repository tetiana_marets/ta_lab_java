package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.ReservationEntity;
import com.epam.lab.hw16.service.ReservationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReservationPresenter {
    private static final Logger log = LogManager.getLogger(ReservationPresenter.class);

    public void showAllReservations (){
        ReservationService reservationService = new ReservationService();
        if (reservationService.findAllReservations().isEmpty()) {
            log.debug("Reservation list is empty");
        }
        else {
            log.info("All available reservations");
            for (ReservationEntity reservation: reservationService.findAllReservations()) {
                log.info(reservation);
            }
        }
    }
}
