package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.CustomerEntity;
import com.epam.lab.hw16.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomerPresenter {
    private static final Logger log = LogManager.getLogger(CustomerPresenter.class);
    public void showAllCustomers () {
        CustomerService customerService = new CustomerService();
        if (customerService.findAllCustomers().isEmpty()) {
            log.debug("Customer list is empty");
        }
        else {
            log.debug("List of all customers");
            for (CustomerEntity customer: customerService.findAllCustomers()) {
                log.info(customer);
            }
        }
    }
}
