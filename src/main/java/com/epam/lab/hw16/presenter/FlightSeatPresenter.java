package com.epam.lab.hw16.presenter;

import com.epam.lab.hw16.model.FlightSeatEntity;
import com.epam.lab.hw16.service.ReservationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class FlightSeatPresenter {
    private static final Logger log = LogManager.getLogger(FlightSeatPresenter.class);

    public void showAvailableSeats (String flightID) {
        log.info("All available seats");
        List<FlightSeatEntity> reservationSeats = new ReservationService().showAvailableSeats(flightID);
        for (FlightSeatEntity flightSeat: reservationSeats) {
            log.info(flightSeat);
        }
    }
}
