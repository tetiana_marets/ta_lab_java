package com.epam.lab.hw16.dbconnection;

import com.epam.lab.hw16.pathfinder.PathFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static final Logger log = LogManager.getLogger(DatabaseConnection.class);
    private static Connection connection = null;

    private DatabaseConnection() {
    }

    public static Connection getConnection() {
        if (connection == null){
            try {
                PathFinder path = new PathFinder();
                connection = DriverManager.getConnection(path.getPathToDB(), path.getUserName(), path.getUserPassword());
            } catch (SQLException ex) {
                log.info("Database Connection Creation Failed : {}",ex.getMessage());
            }
        }
        return connection;
    }
}