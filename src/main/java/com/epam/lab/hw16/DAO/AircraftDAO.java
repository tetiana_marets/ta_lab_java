package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.AircraftEntity;

public interface AircraftDAO extends GeneralDAO<AircraftEntity,Integer> {
}
