package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.FlightDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.FlightEntity;
import com.epam.lab.hw16.model.FlightStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlightDAOImpl implements FlightDAO {
    private static final Logger log = LogManager.getLogger(FlightDAOImpl.class);
    private static final String GET_ALL_FLIGHTS = "SELECT * FROM flight";
    private static final String GET_FLIGHT_BY_ID = "SELECT id, departure_airport_id, arrival_airport_id, departure_date," +
                                                   " departure_time, gate, status, aircraft_id FROM flight \n" +
                                                   "WHERE id = ?";
    private static final String FIND_BY_DEPARTURE = "SELECT * FROM flight where departure_airport_id = ?";
    private static final String FIND_BY_DESTINATION = "SELECT * FROM flight where arrival_airport_id = ?";
    private static final String FIND_BY_STATUS = "SELECT * FROM flight WHERE status = ?";
    private static final String CREATE = "INSERT INTO flight VALUES (?,?,?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE flight SET departure_airport_id = ?, arrival_airport_id = ?, departure_date = ?,\n" +
                                         " departure_time = ?, gate = ?, status = ?, aircraft_id = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM flight WHERE id = ?";
    private static final String SEAT_COUNT_ON_FLIGHT = "SELECT total_seats FROM aircraft \n" +
                                                       "join flight\n" +
                                                       "on aircraft.id = flight.aircraft_id\n" +
                                                       "where flight.id = ?";
    private static final String IF_FLIGHT_EXIST = "SELECT EXISTS(SELECT * from flight WHERE id = ?)";

    @Override
    public List<FlightEntity> getByDestination(String destination) {
        List<FlightEntity> flights = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DESTINATION)){
            ps.setString(1,destination);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightEntity flight = new FlightEntity();
                    flight.setId(rs.getString(1));
                    flight.setDeparture_airport_id(rs.getString(2));
                    flight.setArrival_airport_id(rs.getString(3));
                    flight.setDeparture_date(rs.getDate(4).toLocalDate());
                    flight.setDeparture_time(rs.getTime(5).toLocalTime());
                    flight.setGate(rs.getString(6));
                    flight.setStatus(FlightStatus.valueOf(rs.getString(7).toUpperCase()));
                    flight.setAircraft_id(rs.getInt(8));
                    flights.add(flight);
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return flights;
    }

    @Override
    public List<FlightEntity> getByDeparture(String departure) {
        List<FlightEntity> flights = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DEPARTURE)){
            ps.setString(1,departure);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightEntity flight = new FlightEntity();
                    flight.setId(rs.getString(1));
                    flight.setDeparture_airport_id(rs.getString(2));
                    flight.setArrival_airport_id(rs.getString(3));
                    flight.setDeparture_date(rs.getDate(4).toLocalDate());
                    flight.setDeparture_time(rs.getTime(5).toLocalTime());
                    flight.setGate(rs.getString(6));
                    flight.setStatus(FlightStatus.valueOf(rs.getString(7).toUpperCase()));
                    flight.setAircraft_id(rs.getInt(8));
                    flights.add(flight);
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return flights;
    }

    @Override
    public List<FlightEntity> getByStatus(FlightStatus status) {
        List<FlightEntity> flights = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_STATUS)){
            ps.setString(1,status.toString());
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightEntity flight = new FlightEntity();
                    flight.setId(rs.getString(1));
                    flight.setDeparture_airport_id(rs.getString(2));
                    flight.setArrival_airport_id(rs.getString(3));
                    flight.setDeparture_date(rs.getDate(4).toLocalDate());
                    flight.setDeparture_time(rs.getTime(5).toLocalTime());
                    flight.setGate(rs.getString(6));
                    flight.setStatus(FlightStatus.valueOf(rs.getString(7).toUpperCase()));
                    flight.setAircraft_id(rs.getInt(8));
                    flights.add(flight);
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return flights;
    }

    @Override
    public int getCountOfSeats(String id) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(SEAT_COUNT_ON_FLIGHT)) {
            ps.setString(1,id);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    return rs.getInt(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<FlightEntity> getAll() {
        // Liskov substitution principe
        List<FlightEntity> flights = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_FLIGHTS)){
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightEntity flight = new FlightEntity();
                    flight.setId(rs.getString(1));
                    flight.setDeparture_airport_id(rs.getString(2));
                    flight.setArrival_airport_id(rs.getString(3));
                    flight.setDeparture_date(rs.getDate(4).toLocalDate());
                    flight.setDeparture_time(rs.getTime(5).toLocalTime());
                    flight.setGate(rs.getString(6));
                    flight.setStatus(FlightStatus.valueOf(rs.getString(7).toUpperCase()));
                    flight.setAircraft_id(rs.getInt(8));
                    flights.add(flight);
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return flights;
    }

    @Override
    public FlightEntity getEntityById(String flightID) {
        FlightEntity flight = new FlightEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_FLIGHT_BY_ID)){
            ps.setString(1,flightID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    flight.setId(rs.getString(1));
                    flight.setDeparture_airport_id(rs.getString(2));
                    flight.setArrival_airport_id(rs.getString(3));
                    flight.setDeparture_date(rs.getDate(4).toLocalDate());
                    flight.setDeparture_time(rs.getTime(5).toLocalTime());
                    flight.setGate(rs.getString(6));
                    flight.setStatus(FlightStatus.valueOf(rs.getString(7).toUpperCase()));
                    flight.setAircraft_id(rs.getInt(8));
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return flight;
    }

    @Override
    public int update(FlightEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)){
            ps.setString(1,entity.getDeparture_airport_id());
            ps.setString(2,entity.getArrival_airport_id());
            ps.setDate(3,Date.valueOf(entity.getDeparture_date()));
            ps.setTime(4,Time.valueOf(entity.getDeparture_time()));
            ps.setString(5,entity.getGate());
            ps.setString(6,entity.getStatus().name());
            ps.setInt(7,entity.getAircraft_id());
            ps.setString(8,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(String id) {
        Connection connection = DatabaseConnection.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setString(1,id);
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(FlightEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getId());
            ps.setString(2,entity.getDeparture_airport_id());
            ps.setString(3,entity.getArrival_airport_id());
            ps.setDate(4,Date.valueOf(entity.getDeparture_date()));
            ps.setTime(5,Time.valueOf(entity.getDeparture_time()));
            ps.setString(6,entity.getGate());
            ps.setString(7,entity.getStatus().name());
            ps.setInt(8,entity.getAircraft_id());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    public boolean checkFlightExistById (String flightID) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(IF_FLIGHT_EXIST)) {
            ps.setString(1,flightID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()) {
                    return rs.getBoolean(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }
}
