package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.FlightEntity;
import com.epam.lab.hw16.model.FlightStatus;

import java.util.List;

public interface FlightDAO extends GeneralDAO<FlightEntity,String> {
    List<FlightEntity> getByDestination (String destination);
    List<FlightEntity> getByDeparture (String departure);
    List<FlightEntity> getByStatus (FlightStatus status);
    int getCountOfSeats (String id);
}
