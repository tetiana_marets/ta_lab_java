package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.ReservationEntity;

public interface ReservationDAO extends GeneralDAO<ReservationEntity,Integer> {
    String getSeatNumber (int reservationID);
    String getFlightID (int reservationID);
}
