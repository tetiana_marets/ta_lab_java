package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.FlightSeatDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.FlightSeatEntity;
import com.epam.lab.hw16.model.SeatClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlightSeatDAOImpl implements FlightSeatDAO {
    private static final Logger log = LogManager.getLogger(FlightSeatDAOImpl.class);
    private static final String GET_ALL_SEATS = "SELECT * FROM flight_seat";
    private static final String GET_AVAILABLE_SEATS = "SELECT * FROM flight_seat where (booked = false) and (flight_id = ?)";
    private static final String GET_SEAT_BY_ID = "SELECT * FROM flight_seat WHERE id = ?";
    private static final String CREATE = "INSERT INTO flight_seat (number, flight_id, booked, seat_class) VALUES (?,?,?,?)";
    private static final String UPDATE = "UPDATE flight_seat SET number = ?, flight_id = ?, booked = ?, " +
                                         "seat_class = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM flight_seat WHERE id = ?";
    private static final String IF_SEAT_AVAILABLE = "Select booked from flight_seat WHERE (number = ?) and (flight_id = ?)";
    private static final String  UPDATE_SEAT_STATUS= "UPDATE flight_seat SET booked = ? WHERE (number = ?) and (flight_id = ?)";

    @Override
    public List<FlightSeatEntity> getAll() {
        // Liskov substitution principe
        List<FlightSeatEntity> flightSeats = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_SEATS)){
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightSeatEntity flightSeat = new FlightSeatEntity();
                    flightSeat.setId(rs.getInt(1));
                    flightSeat.setNumber(rs.getString(2));
                    flightSeat.setFlight_id(rs.getString(3));
                    flightSeat.setBooked(rs.getBoolean(4));
                    flightSeat.setSeat_class(SeatClass.valueOf(rs.getString(5).toUpperCase()));
                    flightSeats.add(flightSeat);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }

        return flightSeats;
    }

    @Override
    public FlightSeatEntity getEntityById(Integer id) {
        FlightSeatEntity flightSeat = new FlightSeatEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_SEAT_BY_ID)){
            ps.setInt(1,id);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    flightSeat.setId(rs.getInt(1));
                    flightSeat.setNumber(rs.getString(2));
                    flightSeat.setFlight_id(rs.getString(3));
                    flightSeat.setBooked(rs.getBoolean(4));
                    flightSeat.setSeat_class(SeatClass.valueOf(rs.getString(5).toUpperCase()));
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }

        return flightSeat;
    }

    @Override
    public int update(FlightSeatEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1,entity.getNumber());
            ps.setString(2,entity.getFlight_id());
            ps.setBoolean(3,entity.isBooked());
            ps.setString(4,entity.getSeat_class().name());
            ps.setInt(5,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Integer id) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();

        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(FlightSeatEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1,entity.getNumber());
            ps.setString(2,entity.getFlight_id());
            ps.setBoolean(3,entity.isBooked());
            ps.setString(4,entity.getSeat_class().name());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    public List<FlightSeatEntity> showAvailableSeats (String flightID) {
        List<FlightSeatEntity> flightSeats = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_AVAILABLE_SEATS)) {
            ps.setString(1,flightID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    FlightSeatEntity flightSeat = new FlightSeatEntity();
                    flightSeat.setId(rs.getInt(1));
                    flightSeat.setNumber(rs.getString(2));
                    flightSeat.setFlight_id(rs.getString(3));
                    flightSeat.setBooked(rs.getBoolean(4));
                    flightSeat.setSeat_class(SeatClass.valueOf(rs.getString(5).toUpperCase()));
                    flightSeats.add(flightSeat);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return flightSeats;
    }

    public boolean checkIfSeatReserved (String seatNumber, String flightID){
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(IF_SEAT_AVAILABLE)) {
            ps.setString(1,seatNumber);
            ps.setString(2,flightID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()) {
                        return rs.getBoolean(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }

    @Override
    public int bookSeat (String seatNumber, String flightID) {
        return updateSeatStatus(seatNumber,flightID,true);
    }

    @Override
    public int unbookSeat (String seatNumber, String flightID) {
        return updateSeatStatus(seatNumber,flightID,false);
    }

    private int updateSeatStatus (String seatNumber, String flightID, boolean isBooked) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_SEAT_STATUS)) {
            ps.setBoolean(1,isBooked);
            ps.setString(2,seatNumber);
            ps.setString(3,flightID);
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

}