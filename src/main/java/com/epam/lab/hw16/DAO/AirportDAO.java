package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.AirportEntity;

public interface AirportDAO extends GeneralDAO<AirportEntity,String> {
}
