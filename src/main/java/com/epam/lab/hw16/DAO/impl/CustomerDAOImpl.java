package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.CustomerDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.CustomerEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {
    private static final Logger log = LogManager.getLogger(CustomerDAOImpl.class);
    private static final String GET_ALL = "SELECT * FROM customer";
    private static final String GET_BY_NAME = "SELECT * FROM customer WHERE name = ?";
    private static final String GET_ID_BY_NAME = "SELECT id FROM customer WHERE name = ?";
    private static final String GET_BY_ID = "SELECT * FROM customer WHERE id = ?";
    private static final String UPDATE = "UPDATE customer SET name = ?, email = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM customer WHERE id = ?";
    private static final String CREATE = "INSERT INTO customer (name,email) VALUES (?,?)";
    private static final String IF_CUSTOMER_EXIST = "SELECT EXISTS(SELECT * from customer WHERE name = ?)";


    @Override
    public CustomerEntity getByName(String name) {
        CustomerEntity customer = new CustomerEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_BY_NAME)) {
            ps.setString(1,name);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    customer.setId(rs.getInt(1));
                    customer.setName(rs.getString(2));
                    customer.setEmail(rs.getString(3));
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return customer;
    }

    @Override
    public List<CustomerEntity> getAll() {
        // Liskov substitution principe
        List<CustomerEntity> customers = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    CustomerEntity customer = new CustomerEntity();
                    customer.setId(rs.getInt(1));
                    customer.setName(rs.getString(2));
                    customer.setEmail(rs.getString(3));
                    customers.add(customer);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return customers;
    }

    @Override
    public CustomerEntity getEntityById(Integer id) {
        CustomerEntity customer = new CustomerEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    customer.setId(rs.getInt(1));
                    customer.setName(rs.getString(2));
                    customer.setEmail(rs.getString(3));
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return customer;
    }

    @Override
    public int update(CustomerEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1,entity.getName());
            ps.setString(2,entity.getEmail());
            ps.setInt(3,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Integer id) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(CustomerEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getName());
            ps.setString(2,entity.getEmail());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    public boolean checkCustomerExistByName (String customerName) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(IF_CUSTOMER_EXIST)) {
            ps.setString(1,customerName);
            try (ResultSet rs = ps.executeQuery()){
                rs.next();
                return rs.getBoolean(1);
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }

    public int getIdByName (String customerName) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ID_BY_NAME)) {
            ps.setString(1,customerName);
            try (ResultSet rs = ps.executeQuery()){
                rs.next();
                return rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }
}
