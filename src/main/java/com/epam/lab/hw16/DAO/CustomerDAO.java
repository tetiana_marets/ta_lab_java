package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.CustomerEntity;

public interface CustomerDAO extends GeneralDAO<CustomerEntity,Integer> {
    CustomerEntity getByName(String name);
}
