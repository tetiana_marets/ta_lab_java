package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.ReservationDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.ReservationEntity;
import com.twilio.rest.taskrouter.v1.workspace.task.Reservation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.print.DocFlavor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReservationDAOImpl implements ReservationDAO {
    private static final Logger log = LogManager.getLogger(ReservationDAOImpl.class);
    private static final String GET_ALL_RESERVATIONS = "SELECT * FROM reservation";
    private static final String GET_RESERVATION_BY_ID = "SELECT * FROM reservation WHERE id = ?";
    private static final String UPDATE = "UPDATE reservation SET customer_id = ?, flight_id = ?, seat_number = ?" +
                                         "WHERE id = ?";
    private static final String DELETE = "DELETE FROM reservation WHERE id = ?";
    private static final String CREATE = "INSERT INTO reservation (customer_id, flight_id, seat_number) VALUES (?,?,?)";
    private static final String GET_SEAT_BY_RESERVATION_ID = "SELECT seat_number FROM reservation WHERE id = ?";
    private static final String GET_FLIGHT_BY_RESERVATION_ID = "SELECT flight_id FROM reservation WHERE id = ?";
    private static final String GET_RESERVATION_ID = "Select id from reservation WHERE (flight_id = ?) and (seat_number = ?)";
    @Override
    public List<ReservationEntity> getAll() {
        List<ReservationEntity> reservations = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_RESERVATIONS)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    ReservationEntity reservation = new ReservationEntity();
                    reservation.setId(rs.getInt(1));
                    reservation.setCustomer_id(rs.getInt(2));
                    reservation.setFlight_id(rs.getString(3));
                    reservation.setSeat_number(rs.getString(4));
                    reservations.add(reservation);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return reservations;
    }

    @Override
    public ReservationEntity getEntityById(Integer id) {
        ReservationEntity reservation = new ReservationEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_RESERVATION_BY_ID)) {
            ps.setInt(1,id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    reservation.setId(rs.getInt(1));
                    reservation.setCustomer_id(rs.getInt(2));
                    reservation.setFlight_id(rs.getString(3));
                    reservation.setSeat_number(rs.getString(4));
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return reservation;
    }

    @Override
    public int update(ReservationEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getCustomer_id());
            ps.setString(2, entity.getFlight_id());
            ps.setString(3,entity.getSeat_number());
            ps.setInt(4,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Integer id) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(ReservationEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getCustomer_id());
            ps.setString(2,entity.getFlight_id());
            ps.setString(3,entity.getSeat_number());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public String getSeatNumber (int reservationID){
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_SEAT_BY_RESERVATION_ID)) {
            ps.setInt(1,reservationID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    return rs.getString(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return String.valueOf(0);
    }

    @Override
    public String getFlightID (int reservationID){
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_FLIGHT_BY_RESERVATION_ID)) {
            ps.setInt(1,reservationID);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    return rs.getString(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return String.valueOf(0);
    }

    public int getReservationID (ReservationEntity reservation) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_RESERVATION_ID)) {
            ps.setString(1,reservation.getFlight_id());
            ps.setString(2,reservation.getSeat_number());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    return rs.getInt(1);
                }
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }
}
