package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.AircraftDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.AircraftEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AircraftDAOImpl implements AircraftDAO {
    private static final Logger log = LogManager.getLogger(AircraftDAOImpl.class);
    private static final String GET_ALL_AIRCRAFT = "SELECT * FROM aircraft";
    private static final String GET_AIRCRAFT_BY_ID = "SELECT * FROM aircraft WHERE id = ?";
    private static final String CREATE = "INSERT INTO aircraft (model,total_seats) VALUES (?,?)";
    private static final String UPDATE = "UPDATE aircraft SET model = ?, total_seats = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM aircraft WHERE id = ?";
    private static final String IF_AIRCRAFT_EXIST = "SELECT EXISTS(SELECT * from aircraft WHERE id = ?)";
    @Override
    public List<AircraftEntity> getAll() {
        // Liskov substitution principe
        List<AircraftEntity> aircrafts = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_AIRCRAFT)){
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    AircraftEntity aircraft = new AircraftEntity();
                    aircraft.setId(rs.getInt(1));
                    aircraft.setModel(rs.getString(2));
                    aircraft.setTotal_seats(rs.getInt(3));
                    aircrafts.add(aircraft);
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return aircrafts;
    }

    @Override
    public AircraftEntity getEntityById(Integer id) {
        AircraftEntity aircraft = new AircraftEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_AIRCRAFT_BY_ID)){
            ps.setInt(1,id);
            try (ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    aircraft.setId(rs.getInt(1));
                    aircraft.setModel(rs.getString(2));
                    aircraft.setTotal_seats(rs.getInt(3));
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return aircraft;
    }

    @Override
    public int update(AircraftEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)){
            ps.setString(1,entity.getModel());
            ps.setInt(2,entity.getTotal_seats());
            ps.setInt(3,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Integer id) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(AircraftEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getModel());
            ps.setInt(2,entity.getTotal_seats());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    public boolean checkIfExist (int aircraftID) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(IF_AIRCRAFT_EXIST)) {
            ps.setInt(1,aircraftID);
            try (ResultSet rs = ps.executeQuery()){
                rs.next();
                return rs.getBoolean(1);
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }
}
