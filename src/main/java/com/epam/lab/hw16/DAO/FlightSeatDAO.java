package com.epam.lab.hw16.DAO;

import com.epam.lab.hw16.model.FlightSeatEntity;

public interface FlightSeatDAO extends GeneralDAO<FlightSeatEntity,Integer> {
    int bookSeat (String seatNumber, String flightI);
    int unbookSeat (String seatNumber, String flightI);
}
