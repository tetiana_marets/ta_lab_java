package com.epam.lab.hw16.DAO;

import java.util.List;

public interface GeneralDAO<T,K> {
    List<T> getAll();
    T getEntityById(K id);
    int update(T entity);
    int delete(K id);
    int create (T entity);
}
