package com.epam.lab.hw16.DAO.impl;

import com.epam.lab.hw16.DAO.AirportDAO;
import com.epam.lab.hw16.dbconnection.DatabaseConnection;
import com.epam.lab.hw16.model.AirportEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AirportDAOImpl implements AirportDAO {
    private static final Logger log = LogManager.getLogger(AirportDAOImpl.class);
    private static final String GET_ALL_AIRPORTS = "SELECT * FROM airport";
    private static final String GET_AIRPORT_BY_ID = "SELECT * FROM airport WHERE id = ?";
    private static final String CREATE = "INSERT INTO airport VALUES (?,?,?)";
    private static final String UPDATE = "UPDATE airport SET name = ?, location = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM airport WHERE id = ?";
    private static final String IF_AIRPORT_EXIST = "SELECT EXISTS(SELECT * FROM airport WHERE id = ? )";

    @Override
    public List<AirportEntity> getAll(){
        // Liskov substitution principe
        List<AirportEntity> airports = new ArrayList<>();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL_AIRPORTS)){
              try(ResultSet rs = ps.executeQuery()) {
                  while (rs.next()) {
                      AirportEntity airport = new AirportEntity();
                      airport.setId(rs.getString(1));
                      airport.setName(rs.getString(2));
                      airport.setLocation(rs.getString(3));
                      airports.add(airport);
                  }
              }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return airports;
    }

    @Override
    public AirportEntity getEntityById(String id) {
        AirportEntity airport = new AirportEntity();
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(GET_AIRPORT_BY_ID)){
            ps.setString(1,id);
            try(ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    airport.setId(rs.getString(1));
                    airport.setName(rs.getString(2));
                    airport.setLocation(rs.getString(3));
                }
            }
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return airport;
    }

    @Override
    public int update(AirportEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE)){
            ps.setString(1,entity.getName());
            ps.setString(2,entity.getLocation());
            ps.setString(3,entity.getId());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(String id) {
        Connection connection = DatabaseConnection.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setString(1,id);
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    @Override
    public int create(AirportEntity entity) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getId());
            ps.setString(2,entity.getName());
            ps.setString(3,entity.getLocation());
            return ps.executeUpdate();
        }
        catch (SQLException ex){
            log.info(ex.getMessage());
        }
        return 0;
    }

    public boolean checkIfExist (String airportCode) {
        Connection connection = DatabaseConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(IF_AIRPORT_EXIST)) {
            ps.setString(1,airportCode);
            try (ResultSet rs = ps.executeQuery()){
                rs.next();
                return rs.getBoolean(1);
            }
        }
        catch (SQLException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }
}
