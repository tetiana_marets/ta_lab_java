package com.epam.lab.hw16.model;

public class AircraftEntity {
    private int id;
    private String model;
    private int total_seats;
    public AircraftEntity(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getTotal_seats() {
        return total_seats;
    }

    public void setTotal_seats(int total_seats) {
        this.total_seats = total_seats;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", model='" + model + '\'' +
                ", total_seats=" + total_seats;
    }
}
