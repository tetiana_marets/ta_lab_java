package com.epam.lab.hw16.model;

public class ReservationEntity {
    private int id;
    private int customer_id;
    private String flight_id;
    private String seat_number;
    public ReservationEntity() {}

    public ReservationEntity(int id, int customer_id, String flight_id, String seat_number) {
        this.id = id;
        this.customer_id = customer_id;
        this.flight_id = flight_id;
        this.seat_number = seat_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getFlight_id() {
        return flight_id;
    }

    public void setFlight_id(String flight_id) {
        this.flight_id = flight_id;
    }

    public String getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(String seat_number) {
        this.seat_number = seat_number;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", customer_id=" + customer_id +
                ", flight_id='" + flight_id + '\'' +
                ", seat_number='" + seat_number + '\'';
    }
}
