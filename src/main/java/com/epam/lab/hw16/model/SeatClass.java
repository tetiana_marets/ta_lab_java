package com.epam.lab.hw16.model;

import java.util.Random;

public enum SeatClass {
    ECONOMY,
    BUSINESS,
    FIRST;

    private static final SeatClass[] VALUES = values();
    private static final int SIZE = VALUES.length;
    private static final Random RANDOM = new Random();

    public static SeatClass getRandomSeatClass()  {
        return VALUES[RANDOM.nextInt(SIZE)];
    }
}
