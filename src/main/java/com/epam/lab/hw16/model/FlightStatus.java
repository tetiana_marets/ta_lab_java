package com.epam.lab.hw16.model;

public enum FlightStatus {
    ON_SCHEDULE,
    POSTPONED,
    CANCELED
}
