package com.epam.lab.hw16.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class FlightEntity {
    private String id;
    private String departure_airport_id;
    private String arrival_airport_id;
    private LocalDate departure_date;
    private LocalTime departure_time;
    private String gate;
    private FlightStatus status;
    private int aircraft_id;

    public FlightEntity(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeparture_airport_id() {
        return departure_airport_id;
    }

    public void setDeparture_airport_id(String departure_airport_id) {
        this.departure_airport_id = departure_airport_id;
    }

    public String getArrival_airport_id() {
        return arrival_airport_id;
    }

    public void setArrival_airport_id(String arrival_airport_id) {
        this.arrival_airport_id = arrival_airport_id;
    }

    public LocalDate getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(LocalDate departure_date) {
        this.departure_date = departure_date;
    }

    public String getGate() {
        return gate;
    }

    public LocalTime getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(LocalTime departure_time) {
        this.departure_time = departure_time;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public void setStatus(FlightStatus status) {
        this.status = status;
    }

    public int getAircraft_id() {
        return aircraft_id;
    }

    public void setAircraft_id(int aircraft_id) {
        this.aircraft_id = aircraft_id;
    }

    @Override
    public String toString() {
        return  "id='" + id + '\'' +
                ", departure_airport_id='" + departure_airport_id + '\'' +
                ", arrival_airport_id='" + arrival_airport_id + '\'' +
                ", departure_date=" + departure_date +
                ", departure_time=" + departure_time +
                ", gate='" + gate + '\'' +
                ", status=" + status +
                ", aircraft_id=" + aircraft_id;
    }
}
