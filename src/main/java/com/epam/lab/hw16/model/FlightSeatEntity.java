package com.epam.lab.hw16.model;

public class FlightSeatEntity {
    private int id;
    private String number;
    private String flight_id;
    private boolean booked;
    SeatClass seat_class;

    public FlightSeatEntity(){}
    public FlightSeatEntity(int id, String number, String flight_id, boolean booked, SeatClass seat_class) {
        this.id = id;
        this.number = number;
        this.flight_id = flight_id;
        this.booked = booked;
        this.seat_class = seat_class;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFlight_id() {
        return flight_id;
    }

    public void setFlight_id(String flight_id) {
        this.flight_id = flight_id;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public SeatClass getSeat_class() {
        return seat_class;
    }

    public void setSeat_class(SeatClass seat_class) {
        this.seat_class = seat_class;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", number='" + number + '\'' +
                ", flight_id='" + flight_id + '\'' +
                ", booked=" + booked +
                ", seat_class=" + seat_class;
    }
}
