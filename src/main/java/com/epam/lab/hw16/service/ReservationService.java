package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.CustomerDAOImpl;
import com.epam.lab.hw16.DAO.impl.FlightDAOImpl;
import com.epam.lab.hw16.DAO.impl.FlightSeatDAOImpl;
import com.epam.lab.hw16.DAO.impl.ReservationDAOImpl;
import com.epam.lab.hw16.model.FlightSeatEntity;
import com.epam.lab.hw16.model.ReservationEntity;

import java.util.List;

public class ReservationService {
    public List<ReservationEntity> findAllReservations () {
        return new ReservationDAOImpl().getAll();
    }
    public ReservationEntity findReservationById (int id) {
        return new ReservationDAOImpl().getEntityById(id);
    }
    public int createReservation (ReservationEntity reservation) {
        return new ReservationDAOImpl().create(reservation);
    }
    public int updateReservation (ReservationEntity reservation){
        return new ReservationDAOImpl().update(reservation);
    }
    public int deleteReservation (Integer id) {
        ReservationDAOImpl reservation = new ReservationDAOImpl();
        String seatNumber = reservation.getSeatNumber(id);
        String flightID = reservation.getFlightID(id);
        new FlightSeatDAOImpl().unbookSeat(seatNumber,flightID);
        return new ReservationDAOImpl().delete(id);
    }
    public boolean checkFlightExistById (String flightID){
        return new FlightDAOImpl().checkFlightExistById(flightID);
    }
    public boolean checkCustomerExistByName (String customerName) {
        return new CustomerDAOImpl().checkCustomerExistByName(customerName);
    }
    public List<FlightSeatEntity> showAvailableSeats (String flightID) {
        return new FlightSeatDAOImpl().showAvailableSeats(flightID);
    }
    public boolean checkIfSeatReserved (String seatNumber, String flightID) {
        return new FlightSeatDAOImpl().checkIfSeatReserved(seatNumber,flightID);
    }
    public int reserveSeat (String seatNumber, String flightID) {
        return new FlightSeatDAOImpl().bookSeat(seatNumber,flightID);
    }
    public int getCustomerIdByName (String customerName) {
        return new CustomerDAOImpl().getIdByName(customerName);
    }
    public int getReservationID (ReservationEntity reservation) {
        return new ReservationDAOImpl().getReservationID(reservation);
    }
}
