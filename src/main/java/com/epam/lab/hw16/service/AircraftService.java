package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.AircraftDAOImpl;
import com.epam.lab.hw16.model.AircraftEntity;

import java.util.List;

public class AircraftService {
    public List<AircraftEntity> getAll () {
        return new AircraftDAOImpl().getAll();
    }

    public AircraftEntity getAircraftById (Integer id) {
        return new AircraftDAOImpl().getEntityById(id);
    }

    public int createAircraft (AircraftEntity aircraft) {
        return new AircraftDAOImpl().create(aircraft);
    }

    public int deleteAircraft (Integer id){
        return new AircraftDAOImpl().delete(id);
    }

    public int updateAircraft (AircraftEntity aircraft){
        return new AircraftDAOImpl().update(aircraft);
    }
}
