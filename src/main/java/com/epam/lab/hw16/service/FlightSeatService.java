package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.FlightSeatDAOImpl;
import com.epam.lab.hw16.model.FlightSeatEntity;

public class FlightSeatService {

    public int update (FlightSeatEntity flightSeat){
         return new FlightSeatDAOImpl().update(flightSeat);
    }

    public int delete (Integer id) {
        return new FlightSeatDAOImpl().delete(id);
    }

    public int create (FlightSeatEntity flightSeat){
        return new FlightSeatDAOImpl().create(flightSeat);
    }
}
