package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.AirportDAOImpl;
import com.epam.lab.hw16.model.AirportEntity;

import java.util.List;

public class AirportService {
    public List<AirportEntity> getAll(){
        return new AirportDAOImpl().getAll();
    }

    public AirportEntity getAirportByID(String id){
        return new AirportDAOImpl().getEntityById(id);
    }

    public int createAirport(AirportEntity entity){
        return new AirportDAOImpl().create(entity);
    }

    public int update (AirportEntity entity){
        return new AirportDAOImpl().update(entity);
    }

    public int deleteAirport(String id){
        return new AirportDAOImpl().delete(id);
    }
}
