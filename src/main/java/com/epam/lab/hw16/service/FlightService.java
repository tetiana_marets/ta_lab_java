package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.AircraftDAOImpl;
import com.epam.lab.hw16.DAO.impl.AirportDAOImpl;
import com.epam.lab.hw16.DAO.impl.FlightDAOImpl;
import com.epam.lab.hw16.DAO.impl.FlightSeatDAOImpl;
import com.epam.lab.hw16.model.FlightEntity;
import com.epam.lab.hw16.model.FlightSeatEntity;
import com.epam.lab.hw16.model.FlightStatus;
import com.epam.lab.hw16.model.SeatClass;

import java.util.List;

public class FlightService {
    public List<FlightEntity> findFlightByDestination(String destination) {
        return new FlightDAOImpl().getByDestination(destination);
    }

    public List<FlightEntity> findFlightByDeparture(String departure) {
        return new FlightDAOImpl().getByDeparture(departure);
    }

    public List<FlightEntity> findFlightByStatus(FlightStatus status) {
        return new FlightDAOImpl().getByStatus(status);
    }

    public List<FlightEntity> findAllFlights() {
        return new FlightDAOImpl().getAll();
    }

    public FlightEntity findFlightById(String id) {
        return new FlightDAOImpl().getEntityById(id);
    }

    //Composition FlightSeat cannot exist without flights. 1 to N - 1 flight may have may seats.
    public int createFlight(FlightEntity flight) {
        int charA = 65;
        char letter;
        int seatNumberInRow = 6;
        int nOfFlight = new FlightDAOImpl().create(flight);
        if (nOfFlight > 0) {
            for (int i = 0; i < new FlightDAOImpl().getCountOfSeats(flight.getId()); i++) {
                FlightSeatEntity flightSeat = new FlightSeatEntity();
                letter = (char) (charA + i % seatNumberInRow);
                String seatN = String.valueOf(letter) + ( i / seatNumberInRow +1);
                flightSeat.setNumber(seatN);
                flightSeat.setFlight_id(flight.getId());
                flightSeat.setBooked(false);
                flightSeat.setSeat_class(SeatClass.getRandomSeatClass());
                new FlightSeatDAOImpl().create(flightSeat);
            }
        }
        return nOfFlight;
    }

    public int deleteFlight(String id) {
        return new FlightDAOImpl().delete(id);
    }

    public int updateFlightInfo(FlightEntity flight) {
        return new FlightDAOImpl().update(flight);
    }

    public boolean ifAirportExist (String airportCode) {
        return new AirportDAOImpl().checkIfExist(airportCode);
    }
    public boolean ifAircraftExist (int aircraftID) {
        return new AircraftDAOImpl().checkIfExist(aircraftID);
    }
}