package com.epam.lab.hw16.service;

import com.epam.lab.hw16.DAO.impl.CustomerDAOImpl;
import com.epam.lab.hw16.model.CustomerEntity;

import java.util.List;

public class CustomerService {
    public List<CustomerEntity> findAllCustomers () {
        return new CustomerDAOImpl().getAll();
    }
    public CustomerEntity findCustomerByName (String name) {
        return new CustomerDAOImpl().getByName(name);
    }
    public CustomerEntity findCustomerById (Integer id) {
        return new CustomerDAOImpl().getEntityById(id);
    }
    public int createCustomer (CustomerEntity customer) {
        return new CustomerDAOImpl().create(customer);
    }
    public int updateCustomer (CustomerEntity customer) {
        return new CustomerDAOImpl().update(customer);
    }
    public int deleteCustomer (Integer id) {
        return new CustomerDAOImpl().delete(id);
    }
    public boolean ifCustomerExist (String name) {
        return new CustomerDAOImpl().checkCustomerExistByName(name);
    }
}
