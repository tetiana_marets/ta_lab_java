package com.epam.lab.hw16.notification;

public interface Notification {
    void sendNotification(String s);
}
