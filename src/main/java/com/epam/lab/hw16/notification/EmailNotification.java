package com.epam.lab.hw16.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailNotification implements Notification{
    private static final Logger log = LogManager.getLogger(EmailNotification.class);

    @Override
    public void sendNotification(String s) {
        log.error(s);
    }
}
