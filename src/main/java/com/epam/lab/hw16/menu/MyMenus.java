package com.epam.lab.hw16.menu;

import com.epam.lab.hw16.command.*;
import com.epam.lab.hw16.command.showmenu.ShowMainMenuCommand;
import com.epam.lab.hw16.command.showmenu.ShowSubMenuCustomerCommand;
import com.epam.lab.hw16.command.showmenu.ShowSubMenuFlightCommand;
import com.epam.lab.hw16.command.showmenu.ShowSubMenuReservationCommand;
import com.epam.lab.hw16.presenter.CustomerPresenter;
import com.epam.lab.hw16.presenter.FlightPresenter;
import com.epam.lab.hw16.presenter.ReservationPresenter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

// each of my class is implement principe of single responsibility (it focus on a single concern)
public class MyMenus {
    private static final String INCORRECT_INPUT_MESSAGE = "Your input is incorrect";
    private static final Logger log = LogManager.getLogger(MyMenus.class);
    private static Scanner sc = new Scanner(System.in);
    public static void showMyMenu () {
        new ShowMainMenuCommand().execute();
        mainMenuIteration();
    }
    private static void mainMenuIteration () {
        String choice;
        do {
            choice = sc.nextLine().toUpperCase();
            switch (choice) {
                case "1": {
                    reservationSubMenu();
                    break;
                }
                case "2": {
                    flightSubMenu();
                    break;
                }
                case "3": {
                    customerSubMenu();
                    break;
                }
                case "Q": {
                    System.exit(0);
                }
                default: {
                    log.info(INCORRECT_INPUT_MESSAGE);
                    new ShowMainMenuCommand().execute();
                    break;
                }
            }
        }
        while (true);
    }
    private static void reservationSubMenu () {
        new ShowSubMenuReservationCommand().execute();
        reservationSubMenuIteration();
    }
    private static void reservationSubMenuIteration (){
        String choice;
        do {
            choice = sc.nextLine();
            switch (choice) {
                case "1": {
                    new ReservationPresenter().showAllReservations();
                    new ShowSubMenuReservationCommand().execute();
                    break;
                }
                case "2": {
                    new MakeReservationCommand().execute();
                    new ShowSubMenuReservationCommand().execute();
                    break;
                }
                case "3": {
                    new CancelReservationCommand().execute();
                    new ShowSubMenuReservationCommand().execute();
                    break;
                }
                case "4": {
                    showMyMenu();
                    break;
                }
                default: {
                    log.info(INCORRECT_INPUT_MESSAGE);
                    new ShowSubMenuReservationCommand().execute();
                }
            }
        }
        while (!choice.equalsIgnoreCase("4"));
    }
    private static void flightSubMenu () {
        new ShowSubMenuFlightCommand().execute();
        flightSubMenuIteration();
    }
    private static void flightSubMenuIteration () {
        String choice;
        do {
            choice = sc.nextLine();
            switch (choice) {
                case "1": {
                    new FlightPresenter().showAllFlights();
                    new ShowSubMenuFlightCommand().execute();
                    break;
                }
                case "2": {
                    new FindFlightByDepartureCommand().execute();
                    new ShowSubMenuFlightCommand().execute();
                    break;
                }
                case "3": {
                    new FindFlightByArrivalCommand().execute();
                    new ShowSubMenuFlightCommand().execute();
                    break;
                }
                case "4": {
                    new CreateFlightCommand().execute();
                    new ShowSubMenuFlightCommand().execute();
                    break;
                }
                case "5": {
                    showMyMenu();
                    break;
                }
                default: {
                    log.info(INCORRECT_INPUT_MESSAGE);
                    new ShowSubMenuFlightCommand().execute();
                }
            }
        }
        while (!choice.equalsIgnoreCase("5"));
    }
    private static void customerSubMenu () {
        new ShowSubMenuCustomerCommand().execute();
        customerSubMenuIteration();
    }
    private static void customerSubMenuIteration () {
        String choice;
        do {
            choice = sc.nextLine();
            switch (choice) {
                case "1": {
                    new CustomerPresenter().showAllCustomers();
                    new ShowSubMenuCustomerCommand().execute();
                    break;
                }
                case "2": {
                    new CreateCustomerCommand().execute();
                    new ShowSubMenuCustomerCommand().execute();
                    break;
                }
                case "3": {
                    showMyMenu();
                    break;
                }
                default: {
                    log.info(INCORRECT_INPUT_MESSAGE);
                    new ShowSubMenuCustomerCommand().execute();
                }
            }
        }
        while (!choice.equalsIgnoreCase("3"));
    }
}
