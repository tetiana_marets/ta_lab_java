package com.epam.lab.hw9.mathstream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class Demo {
    private static final Logger log = LogManager.getLogger(Demo.class);

    public static void main(String[] args) {
        DataProvider dataProvider = new DataProvider();
        Integer[] arrInt = dataProvider.fillArray();
        List<Integer> listInt = dataProvider.fillList();
        MathOperation mathOperationList = new MathOperation(listInt);
        MathOperation mathOperationArray = new MathOperation(arrInt);
        mathOperationList.collectStatistics();
        outputDataForList(listInt,mathOperationList);
        outputDataForArray(arrInt,mathOperationArray);
    }

    private static void outputDataForList(List<Integer> listInt, MathOperation mathOperation){
        log.info("Stream created from List:");
        log.info(listInt.toString());
        log.info("Max value is: " + mathOperation.getMaxValue());
        log.info("Min value is: " + mathOperation.getMinValue());
        log.info("Average value is: " + mathOperation.getAverage());
        log.info("Sum of elements is: " + mathOperation.getSum());
        log.info("Sum of elements with reduce function is: " +mathOperation.getSumReduce());
    }
    private static void outputDataForArray(Integer[] arrInt, MathOperation mathOperation){
        log.info("Stream created from Array:");
        log.info(Arrays.toString(arrInt));
        log.info("Number of values that are bigger than average: " + mathOperation.numberBiggerThanAverage());
    }
}
