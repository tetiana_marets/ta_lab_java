package com.epam.lab.hw9.mathstream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

 class DataProvider {
    private static final int MIN_NUMBER = -100;
    private static final int MAX_NUMBER = 100;
    private static final int MAX_NUMBER_OF_ELEMENTS = 10;
    private int size;
    private Random random;
    private Integer[] arrInteger;
    private List<Integer> listInteger;

     DataProvider(){
        random = new Random();
        size = random.nextInt(MAX_NUMBER_OF_ELEMENTS+1);
        arrInteger = new Integer[size];
        listInteger = new ArrayList<>();
    }

     Integer[] fillArray(){
        for (int i = 0; i < size; i++){
            arrInteger[i] = random.nextInt((MAX_NUMBER - MIN_NUMBER) + 1);
        }
        return arrInteger;
    }

     List<Integer> fillList(){
         listInteger = Arrays.asList(arrInteger);
         return listInteger;
    }
}
