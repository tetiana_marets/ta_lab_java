package com.epam.lab.hw9.mathstream;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Stream;

class MathOperation {
    private List<Integer> listInt;
    private Integer[] arrInt;
    private Stream<Integer> streamFromList;
    private Stream<Integer> streamFromArray;
    private IntSummaryStatistics summaryStatistics;

    MathOperation(List<Integer> listInt){
        this.listInt = listInt;
        streamFromList = this.listInt.stream();
    }

    MathOperation(Integer[] arrInt){
        this.arrInt = arrInt;
        streamFromArray = Arrays.stream(this.arrInt);
    }

    void collectStatistics(){
        summaryStatistics = streamFromList.mapToInt(n -> n).summaryStatistics();
    }

    int getMaxValue(){
        return summaryStatistics.getMax();
    }

    int getMinValue(){
        return summaryStatistics.getMin();
    }

    int getSum(){
        return (int)summaryStatistics.getSum();
    }

    double getAverage(){
        return summaryStatistics.getAverage();
    }

    int getSumReduce(){
        return listInt.stream().mapToInt(s->s).reduce(Integer::sum).orElse(0);
    }

    long numberBiggerThanAverage(){
        double average = Arrays.stream(arrInt).mapToInt(s->s).average().orElse(0);
        return streamFromArray.filter(s-> s > average).count();
    }

}
