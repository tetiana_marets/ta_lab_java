package com.epam.lab.hw9.mathstream;

import java.lang.ref.SoftReference;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest {
    public static void main(String[] args) {
       Stream<Number> streamOf = Stream.of(1, 10, 20,300,400,500,2,8,9.10);
       List<Integer> intList = Arrays.asList(4,5,6,7,10,1000,24,33,66,77,88,99);
       Stream<Integer> intStream = intList.stream();
       Integer[] intArray = {10,25,44,66,88,99,134,128};
       Stream<Integer> arrStream = Arrays.stream(intArray);
       IntSummaryStatistics stats = intStream.mapToInt(s-> s).summaryStatistics();
        System.out.println(stats.getAverage());

    }
}
