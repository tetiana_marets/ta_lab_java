package com.epam.lab.hw9.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class CommandMainClass {
    private static final Scanner sc = new Scanner(System.in);
    private static final Logger log = LogManager.getLogger(CommandMainClass.class);
    private static String commandName;
    private static String computerName;

    public static void main(String[] args) {
        Options option = new Options();
        enterInformation();
        option.selectOptions(commandName,computerName);
    }
    private static void enterInformation(){
        try {
            log.info("Please enter command  to START / STOP / RESET / SHUTDOWN computer and name of the computer.");
            log.info("Command:");
            commandName = sc.nextLine().toUpperCase();
            log.info("Computer name:");
            computerName = sc.nextLine();
        }
        catch (IllegalArgumentException e){
            log.info("Please do once again.");
        }
    }
}
