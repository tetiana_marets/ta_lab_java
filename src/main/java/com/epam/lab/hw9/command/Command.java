package com.epam.lab.hw9.command;

@FunctionalInterface
public interface Command {
    void execute (String s);
}
