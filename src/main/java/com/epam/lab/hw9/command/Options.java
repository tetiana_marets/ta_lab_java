package com.epam.lab.hw9.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Options {
    private static final Logger log = LogManager.getLogger(Options.class);
    private List<Command> commandList = new ArrayList<>();

    public void selectOptions(String commandName, String computerName){
        switch (commandName){
            case "START":{
                CommandImplementation startCommand = new CommandImplementation();
                startCommand.execute(computerName);
                break;
            }
            case "STOP": {
                Command command = n -> System.out.println("Computer " + n + " is stopped."
                        + " Command is done with lambda function.");
                command.execute(computerName);
                break;
            }
            case "RESET": {
               Command command = new Command() {
                    @Override
                    public void execute(String s) {
                        log.info("Computer" + s + " is reset." + " Command is done with anonymous class.");
                    }
                };
                command.execute(computerName);
                break;
            }
            case "SHUTDOWN": {
                commandList.add(new CommandImplementation()::shutDown);
                commandList.forEach(n -> n.execute(computerName));
                break;
            }
            default: {
                log.info("You have entered not existing option.");
            }
        }
    }
}