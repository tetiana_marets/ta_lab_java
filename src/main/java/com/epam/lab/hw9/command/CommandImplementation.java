package com.epam.lab.hw9.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandImplementation implements Command {
    private static final Logger log = LogManager.getLogger(CommandImplementation.class);

    void shutDown (String computerName) {
        log.info("Computer " + computerName + " is reset." + " Command is done with method reference.");
    }

    @Override
    public void execute (String computerName) {
        log.info("Computer " + computerName + " is started." + " Command is done with object of command class.");

    }
}
