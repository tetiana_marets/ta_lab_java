package com.epam.lab.hw9.lambda;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class LambdaClass {
    private static final Logger log = LogManager.getLogger(LambdaClass.class);

    public static void main(String[] args) {
        Arithmetical max = (a,b,c) -> maxValue(a,b,c);
        log.info("The max number is: " + max.math(10,30,20));

        Arithmetical average = (a,b,c) -> { return (int)((a + b +c) / 3); };
        log.info("Average is: " + average.math(15,20,30));
    }
    private static int maxValue(int a, int b, int c){
        if ((a>b) && (a>c)) {
            return a;
        }
        else if ((b>c) && (b>a)){
            return b;
        }
        else {
            return c;
        }
    }
}
@FunctionalInterface
interface Arithmetical {
   int math (int a, int b, int c);
}
