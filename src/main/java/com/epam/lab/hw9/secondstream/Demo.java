package com.epam.lab.hw9.secondstream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

public class Demo {
    private static final Logger log = LogManager.getLogger(Demo.class);

    public static void main(String[] args) {
        ArrayList<String> inputLines;
        log.info("Please enter some text line. Finish with empty line.");
        inputLines = InputInformation.inputData();
        StrOperation strOperation = new StrOperation();
        log.info("List of all words: " + strOperation.listOfWords(inputLines));
        log.info("List of all unique words in sorted order: " + strOperation.listOfUniqueWords(inputLines));
        log.info("Number of unique words: " + strOperation.numberOfUniqueWords(inputLines));
        log.info("Words + count: " + strOperation.wordCount(inputLines));
        log.info("Characters + count: " + strOperation.numberOfLowerCaseCharacters(inputLines));
     }
}
