package com.epam.lab.hw9.secondstream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

class StrOperation {
    List<String> listOfWords(ArrayList<String> listOfWords) {
        return listOfWords.stream().map(str -> str.split(" "))
                .flatMap(Arrays::stream)
                .filter(str -> !str.isEmpty())
                .collect(Collectors.toList());
    }
    List<String> listOfUniqueWords(ArrayList<String> listOfWords){
        return listOfWords.stream().map(str -> str.split(" "))
                .flatMap(Arrays::stream).sorted()
                .filter(str -> !str.isEmpty()).distinct()
                .collect(Collectors.toList());
    }
    long numberOfUniqueWords(ArrayList<String> listOfWords){
        return listOfWords.stream().map(str -> str.split(" "))
                .flatMap(Arrays::stream)
                .filter(str -> !str.equals("")).distinct()
                .count();
    }
    Map<String,Long> wordCount(ArrayList<String> listOfWords){
        return listOfWords.stream()
                .map(str->str.split(" "))
                .flatMap(Arrays::stream)
                .filter(str->!str.isEmpty())
                .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
    }
    Map<Character,Long> numberOfLowerCaseCharacters(ArrayList<String> listOfWords){
        String string = listOfWords.toString();
        return string.chars()
                .mapToObj(c->(char)c)
                .filter(Character::isLowerCase)
                .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
    }

}
