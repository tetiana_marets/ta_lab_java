package com.epam.lab.hw9.secondstream;

import java.util.ArrayList;
import java.util.Scanner;

class FileReader {

    static ArrayList<String> inputData() {
        ArrayList<String> listOfWords = new ArrayList<>();
        String input;
        Scanner sc = new Scanner(System.in);
        do{
            input = sc.nextLine();
            listOfWords.add(input);
        } while (!input.isEmpty());
        return listOfWords;
    }

}
