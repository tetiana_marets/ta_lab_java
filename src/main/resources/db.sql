UNLOCK TABLES;
DROP DATABASE IF EXISTS AirportDB;
CREATE DATABASE AirportDB;
USE AirportDB;

CREATE TABLE `airport` (
  `id` varchar(255) PRIMARY KEY,
  `name` varchar(255),
  `location` varchar(255)
)DEFAULT CHARSET=utf8;
LOCK TABLES `airport` WRITE;
INSERT INTO `airport` VALUES ('LWO','Lviv Danylo Halytskyi International Airport','Львів'),
                             ('JFK','John F. Kennedy International Airport','NY'),
                             ('YYZ','Toronto Pearson Airport','Toronto'),
                             ('KBP','Boryspil International Airport','Київ'),
                             ('CDG','Paris Charles de Gaulle','Paris');
UNLOCK TABLES;

CREATE TABLE `flight` (
  `id` varchar(255) PRIMARY KEY NOT NULL,
  `departure_airport_id` varchar(255) DEFAULT '',
  `arrival_airport_id` varchar(255) DEFAULT '',
  `departure_date` date DEFAULT (CURRENT_DATE + INTERVAL 1 MONTH),
  `departure_time` time DEFAULT (CURRENT_TIME),
  `gate` varchar(255) DEFAULT 'G0',
  `status` ENUM ('On_Schedule','Postponed', 'Canceled') DEFAULT 'On_Schedule',
  `aircraft_id`  int
) DEFAULT CHARSET=utf8;
LOCK TABLES `flight` WRITE;
INSERT INTO `flight` VALUES ('FB4529','LWO','JFK','2020-03-08','14:55','G5','CANCELED',1),
                            ('FB4830','KBP','CDG','2020-03-21','22:50','G8','ON_SCHEDULE',2);
UNLOCK TABLES;

CREATE TABLE `reservation` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `customer_id` int,
  `flight_id` varchar(255),
  `seat_number` varchar(255)
)DEFAULT CHARSET=utf8;
LOCK TABLES `reservation` WRITE;
INSERT INTO `reservation` (customer_id,flight_id,seat_number) values (1,'FB4830','A3');
UNLOCK TABLES;

CREATE TABLE `aircraft` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `model` varchar(255),
  `total_seats` int
)DEFAULT CHARSET=utf8;
LOCK TABLES `aircraft` WRITE;
INSERT INTO `aircraft` (model,total_seats) VALUES ('Aerobus-380',5),('Boeing-777',3),('Aerobus-350',4);
UNLOCK TABLES;

CREATE TABLE `flight_seat` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `number` varchar(255) DEFAULT '',
  `flight_id`  varchar(255) NOT NULL DEFAULT '',
  `booked` boolean DEFAULT false,
  `seat_class` ENUM ('Economy', 'Business', 'First') DEFAULT 'First'
)DEFAULT CHARSET=utf8;
LOCK TABLES flight_seat WRITE;
INSERT INTO flight_seat (number,flight_id,booked,seat_class) VALUES ('A1','FB4830',false,'First'), ('A2','FB4830',false,'Economy'),
                                                                    ('A3','FB4830',true,'Business'), ('A1','FB4529',false,'Economy'),
                                                                    ('A2','FB4529',false,'Economy'), ('A3','FB4529',false,'Business'),
                                                                    ('A4','FB4529',false,'First'), ('A5','FB4529',false,'Economy');
UNLOCK TABLES;

CREATE TABLE `customer` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255),
  `email` varchar(255)
) DEFAULT CHARSET=utf8;
LOCK TABLES `customer` WRITE;
INSERT INTO `customer`(name, email) VALUES ('Sonic','1@gmail.com'),('Peppa Pig','2@gmail.com');
UNLOCK TABLES;

ALTER TABLE `flight` ADD FOREIGN KEY (`departure_airport_id`) REFERENCES `airport` (`id`);
ALTER TABLE `flight` ADD FOREIGN KEY (`arrival_airport_id`) REFERENCES `airport` (`id`);
ALTER TABLE `flight` ADD FOREIGN KEY (`aircraft_id`) REFERENCES `aircraft` (`id`);
ALTER TABLE `reservation` ADD FOREIGN KEY (`flight_id`) REFERENCES `flight` (`id`);
ALTER TABLE `reservation` ADD FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
ALTER TABLE `flight_seat` ADD FOREIGN KEY (`flight_id`) REFERENCES `flight` (`id`);